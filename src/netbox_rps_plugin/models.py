"""Models definitions"""

from typing import Any
from datetime import datetime
from urllib.parse import urlparse
from django.core.exceptions import ValidationError  # pylint: disable=import-error
from django.conf import settings
from django.db import models  # pylint: disable=import-error
from django.db.models import Model, Q  # pylint: disable=import-error
from django.urls import reverse  # pylint: disable=import-error
from django.core.validators import (
    URLValidator,
    MaxValueValidator,
    MinValueValidator,
)  # pylint: disable=import-error
from django.contrib.postgres.fields.array import (
    ArrayField,
)  # pylint: disable=import-error
from netbox.models import NetBoxModel  # pylint: disable=import-error, no-name-in-module
from utilities.choices import (
    ChoiceSet,
)  # pylint: disable=import-error, wrong-import-order
from netfields import InetAddressField

URL_MAX_SIZE = 2000
DEFAULT_SORRY_PAGE = settings.PLUGINS_CONFIG["netbox_rps_plugin"]["default_sorry_page"]


def clean_url(raw_url):
    """Clean an URL"""

    o = urlparse(raw_url)

    credential = ":".join(
        tuple(filter(lambda item: item is not None, (o.username, o.password)))
    )

    hostname = o.hostname if o.port is None else ":".join((o.hostname, str(o.port)))

    return (
        o._replace(netloc=hostname)
        if len(credential) == 0
        else o._replace(netloc="@".join((credential, hostname)))
    ).geturl()


class FilteredURLField(models.URLField):
    """URLField definition class"""

    def clean(self, value: Any, model_instance: Model | None) -> Any:
        """Clean Field value"""

        return clean_url(super().clean(value, model_instance))


class MaxBodyChoices(ChoiceSet):
    """Client max body size choices definition class"""

    key = "Mapping.client_max_body_size"

    DEFAULT_VALUE = 128

    CHOICES = [
        (1, "1", "blue"),
        (128, "128", "blue"),
        (512, "512", "blue"),
        (1024, "1024", "blue"),
        (2048, "2048", "blue"),
    ]


class AuthenticationChoices(ChoiceSet):
    """Authentication choices definition class"""

    key = "Mapping.authentication"

    DEFAULT_VALUE = "none"

    CHOICES = [
        ("none", "None", "dark"),
        ("ldap", "Ldap", "blue"),
        ("ecas", "Ecas", "blue"),
    ]


class ExtensionChoices(ChoiceSet):
    """Extension choices definition class"""

    key = "CacheConfig.list_extensions"

    CHOICES = [
        ("JPG", "JPG"),
        ("GIF", "GIF"),
        ("CSS", "CSS"),
        ("HTML", "HTML"),
        ("JS", "JS"),
        ("XML", "XML"),
    ]


class ApplyToChoices(ChoiceSet):
    """Apply to choices definition class"""

    key = "HttpHeader.applyTo"

    DEFAULT_VALUE = "request"

    CHOICES = [
        ("request", "Request", "blue"),
        ("response", "Response", "red"),
    ]


class RedirectTypeChoices(ChoiceSet):
    """Redirect type definition class"""

    key = "Redirect.redirect_type"

    DEFAULT_VALUE = "1-1"

    CHOICES = [
        ('1-1', 'One to One - Single page to single page'),
        ('N-N', 'Many to Many - Many pages to many pages behind context path'),
        ('N-1', 'Many to One - Many pages behind context path to a single page'),
    ]


class ExtraProtocolChoices(ChoiceSet):
    """Protocol choices definition class"""

    key = "Mapping.protocol"

    WEBSOCKET_SECURE = "websocket"

    CHOICES = [
        (WEBSOCKET_SECURE, "Websocket", "blue"),
    ]


# Info: No longer in use since last requirements, to be confirmed before delete-it completely.
# class ProxyBufferChoices(ChoiceSet):
#     """Proxy Buffer Selection definition class"""

#     key = "Mapping.proxyBufferSize"

#     DEFAULT_VALUE=4

#     CHOICES = [
#         (4, "4", "dark"),
#         (32, "32", "blue"),
#         (64, "64", "blue"),
#         (128, "128", "blue"),
#     ]


def default_protocol():
    """Return the default protocols"""
    return []


class Mapping(NetBoxModel):
    """Mapping definition class"""

    source = FilteredURLField(
        max_length=URL_MAX_SIZE,
        blank=False,
        verbose_name="Source",
        validators=[URLValidator(schemes=["http", "https"])],
        unique=True,
    )
    target = FilteredURLField(
        max_length=URL_MAX_SIZE,
        blank=False,
        verbose_name="Target",
        validators=[URLValidator(schemes=["http", "https"])],
    )
    authentication = models.CharField(
        max_length=30,
        choices=AuthenticationChoices,
        default=AuthenticationChoices.DEFAULT_VALUE,
        blank=False,
        verbose_name="Auth",
    )
    testingpage = models.CharField(
        max_length=URL_MAX_SIZE,
        blank=True,
        null=True,
    )
    webdav = models.BooleanField(
        default=False,
    )
    Comment = models.CharField(max_length=500, blank=True)
    gzip_proxied = models.BooleanField(default=False)
    keepalive_requests = models.IntegerField(
        default=1000, validators=[MinValueValidator(100), MaxValueValidator(5000)]
    )
    keepalive_timeout = models.IntegerField(
        default=75, validators=[MinValueValidator(1), MaxValueValidator(300)]
    )
    proxy_cache = models.BooleanField(default=False)
    proxy_read_timeout = models.IntegerField(
        default=60, validators=[MinValueValidator(1), MaxValueValidator(300)]
    )
    client_max_body_size = models.IntegerField(
        choices=MaxBodyChoices,
        default=MaxBodyChoices.DEFAULT_VALUE,
    )
    sorry_page = models.URLField(
        max_length=URL_MAX_SIZE,
        blank=False,
        verbose_name="Sorry Page",
        validators=[URLValidator(schemes=["http", "https"])],
        default=DEFAULT_SORRY_PAGE,
    )
    extra_protocols = ArrayField(
        base_field=models.CharField(max_length=32, choices=ExtraProtocolChoices),
        null=False,
        blank=True,
        verbose_name="Extra Protocols",
        default=default_protocol,
    )

    proxy_buffer_size = models.IntegerField(
        default=4,
        validators=[MinValueValidator(4), MaxValueValidator(128)],
        blank=True,
        null=True,
        # choices=ProxyBufferChoices,
    )
    # Info : Will be (x4) proxy_buffer_size
    proxy_buffer = models.IntegerField(
        default=16,
        validators=[MinValueValidator(16), MaxValueValidator(512)],
        blank=True,
        null=True,
    )
    # Info : Will be (x2) proxy_buffer_size
    proxy_busy_buffer = models.IntegerField(
        default=8,
        validators=[MinValueValidator(8), MaxValueValidator(256)],
        blank=True,
        null=True,
    )

    proxy_buffer_responses = models.BooleanField(default=True)

    proxy_buffer_requests = models.BooleanField(default=True)

    class Meta:
        """Meta class"""

        ordering = ("source", "target")
        constraints = [
            models.CheckConstraint(
                check=~models.Q(source__exact=models.F("target")),
                name="%(app_label)s_%(class)s_check_target_source_url",
            )
        ]

    def __str__(self):
        return str(self.source)

    def get_absolute_url(self):
        """override"""
        return reverse("plugins:netbox_rps_plugin:mapping", args=[self.pk])

    def clean(self):
        """Clean model method for validation"""
        super().clean()

        if self.source == self.target:
            raise ValidationError(
                {"target": "Target URL cannot be equal to source URL."}
            )


class SamlConfig(NetBoxModel):
    """SAML config definition class"""

    acs_url = models.CharField(
        max_length=URL_MAX_SIZE,
        blank=False,
        verbose_name="ACS URL",
        validators=[URLValidator(message="It must be a url")],
    )
    logout_url = models.CharField(
        max_length=URL_MAX_SIZE,
        blank=False,
        verbose_name="Logout URL",
        validators=[URLValidator(message="It must be a url")],
    )
    force_nauth = models.BooleanField(
        default=False,
    )
    Mapping = models.OneToOneField(
        Mapping,
        on_delete=models.CASCADE,
        related_name="saml_config",
        db_column="mapping_id",
        name="mapping",
    )


class HttpHeader(NetBoxModel):
    """HTTP Header definition class"""

    mapping = models.ForeignKey(
        Mapping, on_delete=models.CASCADE, related_name="http_headers"
    )
    name = models.CharField(max_length=120, blank=False, verbose_name="Header name")
    value = models.CharField(
        max_length=256, null=True, blank=True, verbose_name="Header value"
    )
    apply_to = models.CharField(
        max_length=30,
        choices=ApplyToChoices,
        default=ApplyToChoices.DEFAULT_VALUE,
        blank=False,
        verbose_name="Apply to",
    )

    class Meta:
        """Meta class"""

        unique_together = ["mapping", "name", "apply_to"]
        ordering = ["name"]

    def __str__(self):
        return str(self.name)

    def get_absolute_url(self):
        """override"""
        return reverse("plugins:netbox_rps_plugin:httpheader", args=[self.pk])


class Redirect(NetBoxModel):
    """Redirect definition class"""

    id = models.AutoField(primary_key=True) # Big (8 byte) integer, Unique ID
    mapping = models.ForeignKey(
        Mapping, on_delete=models.CASCADE, related_name="redirects"
    )

    source = models.CharField(max_length=2000, blank=False)  # String up to 2000 characters
    target = models.CharField(max_length=2000, blank=False)  # String up to 2000 characters

    redirect_type = models.CharField(
        blank=False,
        max_length=3,
        choices=RedirectTypeChoices,
        default=RedirectTypeChoices.DEFAULT_VALUE,
    ) # Array of string, limited values

    activation_date = models.DateTimeField(default=datetime.now)  # Datetime, default is now
    deactivation_date = models.DateTimeField(null=True, blank=True)  # Datetime, can be null

    class Meta:
        """Meta class"""
        constraints = [
            models.CheckConstraint(
                check=models.Q(deactivation_date__gt=models.F('activation_date')) | models.Q(deactivation_date__isnull=True),
                name='deactivation_date_after_activation_date'
            ),
        ]
        ordering = [
            "id",
            "mapping",
            "source",
            "target",
            "redirect_type",
            "activation_date",
            "deactivation_date",
            "last_updated",
            "created"
        ]

    def clean(self):
        """Clean model method for validation"""
        super().clean()

        if self.source == self.target:
            raise ValidationError(
                {"target": "Target URL cannot be equal to source URL."}
            )

        # Ensure deactivation_date is after activation_date
        if self.deactivation_date and self.deactivation_date <= self.activation_date:
            raise ValidationError(
                {"deactivation_date": 'Deactivation date should be after the activation date.'}
            )

        # Ensure the source of the Redirect starts with the source of the Mapping
        if not self.source.startswith(self.mapping.source): # pylint: disable=E1101
            raise ValidationError(
                {"source": "Redirect source must start with the mapping's source."}
            )

        # Ensure no other Redirect exists with the same source in the same timespan
        # Define the conditions for overlapping redirects
        overlap_conditions = (
            Q(mapping=self.mapping) &
            Q(source=self.source) & (
                (
                    Q(activation_date__gte=self.activation_date) &
                    Q(activation_date__lte=self.deactivation_date if self.deactivation_date else datetime.max)
                ) | (
                    Q(activation_date__lte=self.activation_date) &
                    (Q(deactivation_date__gte=self.activation_date) | Q(deactivation_date__isnull=True))
                )
            )
        )

        # Get the overlapping Redirect objects
        overlapping_redirects = Redirect.objects.filter(overlap_conditions).exclude(id=self.id)

        if overlapping_redirects.exists():
            raise ValidationError(
                {"source": "A Redirect with the same source and overlapping timespan already exists."}
            )

    def save(self, *args, **kwargs):
        """Override save method for validation"""
        self.clean()
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        """override"""
        return reverse("plugins:netbox_rps_plugin:redirect", args=[self.pk])

    def __str__(self):
        return str(self.id)


class AclDeniedSource(NetBoxModel):
    """AclDeniedSource class"""

    id = models.AutoField(primary_key=True)
    # acl_source = models.CharField(max_length=18)
    acl_source = InetAddressField()
    last_updated = models.DateTimeField(auto_now_add=True)
    created = models.DateTimeField(auto_now_add=True)

    mapping = models.ForeignKey(
        Mapping,
        on_delete=models.CASCADE,
        related_name="acl_denied_source",
        # db_column="mapping_id",
        name="mapping",
    )

    class Meta:
        """Meta class"""

        ordering = ["id", "mapping", "acl_source", "last_updated", "created"]

    def __str__(self):
        return f"Netbox RPS Plugin ACL Denied Source {self.id}"
        # return f"Netbox RPS Plugin ACL Denied Source {self.id}, {self.mapping}, \
        #     {self.acl_source}, {self.last_updated}, {self.created}"

    def get_absolute_url(self):
        """Get the absolute url

        Returns:
            str: absolute url
        """
        return reverse("plugins:netbox_rps_plugin:acldeniedsource", args=[self.pk])


class CacheConfig(NetBoxModel):
    """Cache Config definition class"""

    id = models.AutoField(primary_key=True)
    mapping = models.ForeignKey(
        Mapping,
        on_delete=models.CASCADE,
        related_name="cache_configs",
        # db_column="mapping_id",
    )

    list_extensions = ArrayField(
        base_field=models.CharField(blank=True, choices=ExtensionChoices)
    )
    ttl = models.IntegerField(default=0)
    max_size_limit = models.IntegerField(
        default=100, validators=[MinValueValidator(1), MaxValueValidator(100)]
    )
    last_updated = models.DateTimeField(auto_now_add=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Cache Config number '{self.id}'"

    def get_absolute_url(self):
        """override"""
        return reverse("plugins:netbox_rps_plugin:cacheconfig", args=[self.id])


class HstsMaxAge(ChoiceSet):
    """Hsts choices definition class"""

    DEFAULT_VALUE = 31536000

    CHOICES = [(0, "no cache"), (31536000, "one year"), (63072000, "two years")]


class HstsProtocol(NetBoxModel):
    """Hsts Protocoal definition class"""

    id = models.AutoField(primary_key=True)
    mapping = models.OneToOneField(
        Mapping,
        on_delete=models.CASCADE,
        related_name="hsts_protocol",
        db_column="mapping_id",
        name="mapping",
    )

    subdomains = models.BooleanField()
    preload_flag = models.BooleanField()
    max_age = models.IntegerField(
        default=HstsMaxAge.DEFAULT_VALUE,
        choices=HstsMaxAge.CHOICES,
    )
    last_updated = models.DateTimeField(auto_now_add=True)
