"""Tables definitions"""

import django_tables2 as tables
from netbox.tables import (
    NetBoxTable,
    ChoiceFieldColumn,
    columns,
)  # pylint: disable=import-error, no-name-in-module
from .models import Mapping, HttpHeader, AclDeniedSource, CacheConfig, HstsProtocol, Redirect


class MappingTable(NetBoxTable):
    """Mapping Table definition class"""

    authentication = ChoiceFieldColumn()
    source = tables.Column(linkify=True)
    httpheader_count = columns.LinkedCountColumn(
        viewname="plugins:netbox_rps_plugin:httpheader_list",
        url_params={"mapping": "pk"},
        verbose_name="HTTP Headers count",
    )
    tags = columns.TagColumn()

    class Meta(NetBoxTable.Meta):
        """Meta class"""

        model = Mapping
        fields = (
            "pk",
            "id",
            "source",
            "target",
            "authentication",
            "testingpage",
            "webdav",
            "extra_protocols",
            "Comment",
            "gzip_proxied",
            "keepalive_requests",
            "keepalive_timeout",
            "proxy_cache",
            "proxy_read_timeout",
            "client_max_body_size",
            "httpheader_count",
            "sorry_page",
            "tags",
            "created",
            "last_updated",
            "proxy_buffer_size",
            "proxy_buffer",
            "proxy_busy_buffer",
            "proxy_buffer_responses",
            "proxy_buffer_requests",
        )
        default_columns = (
            "source",
            "target",
            "authentication",
            "webdav",
            "extra_protocols",
            "gzip_proxied",
            "keepalive_requests",
            "keepalive_timeout",
            "proxy_cache",
            "proxy_read_timeout",
            "client_max_body_size",
            "httpheader_count",
        )


class HttpHeaderTable(NetBoxTable):
    """HTTP header Table definition class"""

    name = tables.Column(linkify=True)

    class Meta(NetBoxTable.Meta):
        """Meta class"""

        model = HttpHeader
        fields = (
            "pk",
            "id",
            "mapping",
            "name",
            "value",
            "apply_to",
            "tags",
            "created",
            "last_updated",
        )
        default_columns = ("mapping", "name", "value", "apply_to")


class RedirectTable(NetBoxTable):
    """Redirect Table definition class"""

    id = tables.Column(linkify=True)
    mapping = tables.Column(linkify=True)

    class Meta(NetBoxTable.Meta):
        """Meta class"""

        model = Redirect
        fields = (
            'id',
            'mapping',
            'source',
            'target',
            'redirect_type',
            'activation_date',
            'deactivation_date',
            'created',
            'last_updated'
        )
        default_columns = ("id", "mapping", "source", "target", "redirect_type", "activation_date", "deactivation_date")


class AclDeniedSourceTable(NetBoxTable):
    """Acl Denied Source Table definition class"""

    class Meta(NetBoxTable.Meta):
        """Meta class"""

        model = AclDeniedSource
        fields = (
            "id",
            "mapping",
            "acl_source",
            "last_updated",
            "created",
        )
        default_columns = (
            "id",
            "mapping",
            "acl_source",
            "last_updated",
            "created",
        )


class CacheConfigTable(NetBoxTable):
    """Cache Config Table definition class"""

    name = tables.Column(linkify=True)

    class Meta(NetBoxTable.Meta):
        """Meta class"""

        model = CacheConfig
        fields = (
            "id",
            "mapping",
            "list_extensions",
            "ttl",
            "max_size_limit",
            "created",
            "last_updated",
        )


class HstsProtocolTable(NetBoxTable):
    """Hsts Protocol Table definition class"""

    name = tables.Column(linkify=True)

    class Meta(NetBoxTable.Meta):
        """Meta class"""

        model = HstsProtocol
        fields = (
            "id",
            "mapping",
            "subdomains",
            "preload_flag",
            "max_age",
        )
