"""Navigation Menu definitions"""

from extras.plugins import PluginMenuButton, PluginMenuItem, PluginMenu # pylint: disable=import-error
from utilities.choices import ButtonColorChoices # pylint: disable=import-error

mapping_buttons = [
    PluginMenuButton(
        link="plugins:netbox_rps_plugin:mapping_add",
        title="Add",
        icon_class="mdi mdi-plus-thick",
        color=ButtonColorChoices.GREEN,
    ),
    PluginMenuButton(
        link="plugins:netbox_rps_plugin:mapping_add",
        title="Import",
        icon_class="mdi mdi-upload",
        color=ButtonColorChoices.CYAN,
    ),
]

rps_items = [
    PluginMenuItem(
        link="plugins:netbox_rps_plugin:mapping_list",
        link_text="Mappings",
        buttons=mapping_buttons,
        permissions=["netbox_rps_plugin.view_mapping"],
    ),
]

menu = PluginMenu(
    label="Reverse Proxy",
    groups=(
        ("RPS", rps_items),
    ),
    icon_class="mdi mdi-graph-outline",
)
