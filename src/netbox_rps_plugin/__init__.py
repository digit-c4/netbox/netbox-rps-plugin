"""Netbox Plugin Configuration"""

from netbox_nms_common.plugin_config import AutoPluginConfig


class NetBoxRpsConfig(AutoPluginConfig):
    """Netbox Plugin Configuration class"""

    name = __package__
    verbose_name = "NetBox RPS"
    base_url = "rps"
    default_settings = {"default_sorry_page": "https://sorry.ec.europa.eu/"}
    django_apps = ["django_extensions"]

    def ready(self):
        "Funtion that runs after initial setup of Netbox"

        # pylint: disable=C0415, W0611:
        from netbox_rps_plugin.signals import (
            mappings,
        )

        super().ready()


# pylint: disable=C0103
config = NetBoxRpsConfig
