from typing import Union
from django.db.models.signals import post_save
from netbox_rps_plugin.models import (
    HttpHeader,
    SamlConfig,
    AclDeniedSource,
    CacheConfig,
    HstsProtocol,
    Redirect
)

mapping_sub_resources = [
    HttpHeader,
    SamlConfig,
    AclDeniedSource,
    CacheConfig,
    HstsProtocol,
    Redirect
]


def callback(
    sender,
    instance: Union[
        HttpHeader,
        SamlConfig,
        AclDeniedSource,
        CacheConfig,
        HstsProtocol,
        Redirect
    ],
    **kwargs
):
    instance.mapping.save()


for sub_resource in mapping_sub_resources:
    post_save.connect(callback, sender=sub_resource)
