"""Model views definitions"""

from netbox.views import generic  # pylint: disable=import-error
from utilities.utils import count_related # pylint: disable=import-error
from utilities.views import ViewTab, register_model_view # pylint: disable=import-error
from django.utils.translation import gettext as _
from netbox_rps_plugin import forms, tables, filtersets, models


class MappingView(generic.ObjectView):
    """Mapping view definition"""

    queryset = (
        models.Mapping.objects.all()
        .prefetch_related("http_headers")
        .prefetch_related("saml_config")
        .prefetch_related("acl_denied_source")
        .prefetch_related("redirects")
        # .prefetch_related("cache_config")
    )


class MappingListView(generic.ObjectListView):
    """Mapping list view definition"""

    queryset = models.Mapping.objects.annotate(
        httpheader_count=count_related(models.HttpHeader, "mapping")
    )
    table = tables.MappingTable
    filterset = filtersets.MappingFilterSet
    filterset_form = forms.MappingFilterForm


class MappingEditView(generic.ObjectEditView):
    """Mapping edition view definition"""

    queryset = models.Mapping.objects.all()
    form = forms.MappingForm


class MappingBulkImportView(generic.BulkImportView):
    """Mapping bulk import view definition"""

    queryset = models.Mapping.objects.all()
    model_form = forms.MappingImportForm


class MappingDeleteView(generic.ObjectDeleteView):
    """Mapping delete view definition"""

    queryset = models.Mapping.objects.all()


class MappingBulkDeleteView(generic.BulkDeleteView):
    """Mapping bulk delete view definition"""

    queryset = models.Mapping.objects.all()
    filterset = filtersets.MappingFilterSet
    table = tables.MappingTable


@register_model_view(models.Mapping, "httpheader")
class MappingHttpHeadersView(generic.ObjectChildrenView):
    """Mapping HTTP Header view definition"""

    queryset = models.Mapping.objects.all().prefetch_related("http_headers")
    child_model = models.HttpHeader
    table = tables.HttpHeaderTable
    filterset = filtersets.HttpHeaderFilterSet
    template_name = "netbox_rps_plugin/httpheader/child.html"

    tab = ViewTab(
        label=_("HTTP Headers"),
        badge=lambda obj: obj.http_headers.count(),
        hide_if_empty=False,
    )

    # pylint: disable=W0613
    def get_children(self, request, parent):
        """override"""
        return parent.http_headers


@register_model_view(models.Mapping, "redirect")
class MappingRedirectsView(generic.ObjectChildrenView):
    """Mapping Redirect view definition"""

    queryset = models.Mapping.objects.all().prefetch_related("redirects")
    child_model = models.Redirect
    table = tables.RedirectTable
    filterset = filtersets.RedirectFilterSet
    template_name = "netbox_rps_plugin/redirect/child.html"

    tab = ViewTab(
        label=_("Redirects"),
        badge=lambda obj: obj.redirects.count(),
        hide_if_empty=False,
    )

    # pylint: disable=W0613
    def get_children(self, request, parent):
        """override"""
        return parent.redirects


@register_model_view(models.Mapping, "samlconfig")
class MappingSamlConfigView(generic.ObjectView):
    """Mapping SAML Config view definition"""

    base_template = "netbox_rps_plugin/mapping.html"
    queryset = models.Mapping.objects.all().prefetch_related("saml_config")
    template_name = "netbox_rps_plugin/saml_config.html"

    tab = ViewTab(
        label=_("SAML Configuration"),
        hide_if_empty=True,
        badge=lambda obj: 1 if hasattr(obj, "saml_config") else 0,
    )


@register_model_view(models.Mapping, "cacheconfig")
class MappingCacheConfigView(generic.ObjectChildrenView):
    """Mapping Cache Config view definition"""

    queryset = models.Mapping.objects.all().prefetch_related("cache_configs")
    child_model = models.CacheConfig
    table = tables.CacheConfigTable
    filterset = filtersets.CacheConfigFilterSet
    template_name = "netbox_rps_plugin/cacheconfig/child.html"

    tab = ViewTab(
        label=_("Cache Configuration"),
        badge=lambda obj: obj.cache_configs.count(),
        hide_if_empty=False,
    )

    # pylint: disable=W0613
    def get_children(self, request, parent):
        """override"""
        return parent.cache_configs

@register_model_view(models.Mapping, "hstsprotocol")
class MappingHstsProtocolView(generic.ObjectView):
    """Mapping Hsts protocol view definition"""

    queryset = models.Mapping.objects.all().prefetch_related("hsts_protocol")
    child_model = models.HstsProtocol
    table = tables.HstsProtocolTable
    filterset = filtersets.HstsProtocolFilterSet
    template_name = "netbox_rps_plugin/hstsprotocol.html"


    tab = ViewTab(
        label=_("HSTS Protocol"),
        badge=lambda obj: 1 if hasattr(obj, "hsts_protocol") else 0,
        hide_if_empty=True,
    )

@register_model_view(models.Mapping, "acldeniedsource")
class MappingAclDeniedSourceView(generic.ObjectChildrenView):
    """Mapping HTTP Header view definition"""

    queryset = models.Mapping.objects.all().prefetch_related("acl_denied_source")
    child_model = models.AclDeniedSource
    table = tables.AclDeniedSourceTable
    filterset = filtersets.AclDeniedSourceFilterSet
    template_name = "netbox_rps_plugin/acldeniedsource/child.html"

    tab = ViewTab(
        label=_("ACL Denied Source"),
        badge=lambda obj: obj.acl_denied_source.count(),
        hide_if_empty=False,
    )

    # pylint: disable=W0613
    def get_children(self, request, parent):
        """override"""
        return parent.acl_denied_source



class HttpHeaderView(generic.ObjectView):
    """HTTP Header view definition"""

    queryset = models.HttpHeader.objects.all()


class HttpHeaderListView(generic.ObjectListView):
    """HTTP Header list view definition"""

    queryset = models.HttpHeader.objects.all()
    table = tables.HttpHeaderTable
    filterset = filtersets.HttpHeaderFilterSet
    filterset_form = forms.HttpHeaderFilterForm


class HttpHeaderEditView(generic.ObjectEditView):
    """HTTP Header edition view definition"""

    queryset = models.HttpHeader.objects.all()
    form = forms.HttpHeaderForm


class HttpHeaderDeleteView(generic.ObjectDeleteView):
    """HTTP Header delete view definition"""

    queryset = models.HttpHeader.objects.all()


class HttpHeaderBulkDeleteView(generic.BulkDeleteView):
    """HTTP Header bulk delete view definition"""

    queryset = models.HttpHeader.objects.all()
    filterset = filtersets.HttpHeaderFilterSet
    table = tables.HttpHeaderTable


class RedirectView(generic.ObjectView):
    """Redirect view definition"""

    queryset = models.Redirect.objects.all()

class RedirectListView(generic.ObjectListView):
    """Redirect list view definition"""

    queryset = models.Redirect.objects.all()
    table = tables.RedirectTable
    filterset = filtersets.RedirectFilterSet
    filterset_form = forms.RedirectFilterForm

class RedirectEditView(generic.ObjectEditView):
    """Redirect edition view definition"""

    queryset = models.Redirect.objects.all()
    form = forms.RedirectForm

class RedirectDeleteView(generic.ObjectDeleteView):
    """Redirect delete view definition"""

    queryset = models.Redirect.objects.all()

class RedirectBulkDeleteView(generic.BulkDeleteView):
    """Redirect bulk delete view definition"""

    queryset = models.Redirect.objects.all()
    filterset = filtersets.RedirectFilterSet
    table = tables.RedirectTable


class SamlConfigEditView(generic.ObjectEditView):
    """HTTP SAML config edition view definition"""

    queryset = models.SamlConfig.objects.all()
    form = forms.SamlConfigForm


class SamlConfigDeleteView(generic.ObjectDeleteView):
    """HTTP SAML config delete view definition"""

    queryset = models.SamlConfig.objects.all()


# ACLDeniedSource
class AclDeniedSourceView(generic.ObjectView):
    """AclDeniedSource view definition"""
    queryset = models.AclDeniedSource.objects.all()


class AclDeniedSourceListView(generic.ObjectListView):
    """AclDeniedSource list view definition"""

    queryset = models.AclDeniedSource.objects.annotate()
    table = tables.AclDeniedSourceTable
    filterset = filtersets.AclDeniedSourceFilterSet
    filterset_form = forms.AclDeniedSourceFilterForm


class AclDeniedSourceEditView(generic.ObjectEditView):
    """AclDeniedSource edition view definition"""

    queryset = models.AclDeniedSource.objects.all()
    form = forms.AclDeniedSourceForm


class AclDeniedSourceBulkDeleteView(generic.BulkDeleteView):
    """HTTP Header bulk delete view definition"""

    queryset = models.AclDeniedSource.objects.all()
    filterset = filtersets.AclDeniedSourceFilterSet
    table = tables.AclDeniedSourceTable

class AclDeniedSourceDeleteView(generic.ObjectDeleteView):
    """AclDeniedSource delete view definition"""

    queryset = models.AclDeniedSource.objects.all()


class CacheConfigView(generic.ObjectView):
    """Cache config view definition"""

    queryset = models.CacheConfig.objects.all()


class CacheConfigListView(generic.ObjectListView):
    """Cache configuration list view definition"""

    queryset = models.CacheConfig.objects.all()
    table = tables.CacheConfigTable
    filterset = filtersets.CacheConfigFilterSet
    filterset_form = forms.CacheConfigFilterForm


class CacheConfigEditView(generic.ObjectEditView):
    """Cache config edition view definition"""

    queryset = models.CacheConfig.objects.all()
    form = forms.CacheConfigForm


class CacheConfigBulkDeleteView(generic.BulkDeleteView):
    """Cache config bulk delete view definition"""

    queryset = models.CacheConfig.objects.all()
    # filterset = filtersets.CacheConfigFilterSet
    table = tables.CacheConfigTable


class CacheConfigDeleteView(generic.ObjectDeleteView):
    """Cache config delete view definition"""

    queryset = models.CacheConfig.objects.all()

class HstsProtocolView(generic.ObjectView):
    """Cache config view definition"""

    queryset = models.HstsProtocol.objects.all()


class HstsProtocolListView(generic.ObjectListView):
    """Cache configuration list view definition"""

    queryset = models.HstsProtocol.objects.all()
    table = tables.HstsProtocolTable
    # filterset = filtersets.HstsProtocolFilterSet
    filterset_form = forms.HstsProtocolFilterForm


class HstsProtocolEditView(generic.ObjectEditView):
    """Cache config edition view definition"""

    queryset = models.HstsProtocol.objects.all()
    form = forms.HstsProtocolForm


class HstsProtocolBulkDeleteView(generic.BulkDeleteView):
    """Cache config bulk delete view definition"""

    queryset = models.HstsProtocol.objects.all()
    # filterset = filtersets.HstsProtocolFilterSet
    table = tables.HstsProtocolTable


class HstsProtocolDeleteView(generic.ObjectDeleteView):
    """Cache config delete view definition"""

    queryset = models.HstsProtocol.objects.all()
