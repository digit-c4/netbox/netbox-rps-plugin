"""Filtersets definitions"""

from django_filters import filters
from netbox.filtersets import NetBoxModelFilterSet  # pylint: disable=import-error
from django.db.models import Q
from netfields import InetAddressField
from .models import HstsProtocol, Mapping, HttpHeader, AclDeniedSource, CacheConfig, Redirect


class MappingFilterSet(NetBoxModelFilterSet):
    """Mapping filterset definition class"""

    extra_protocols = filters.CharFilter(lookup_expr="icontains")

    class Meta:
        """Meta class"""

        model = Mapping
        fields = (
            "id",
            "authentication",
            "source",
            "target",
            "Comment",
            "webdav",
            "extra_protocols",
            "testingpage",
            "gzip_proxied",
            "keepalive_requests",
            "keepalive_timeout",
            "proxy_cache",
            "proxy_read_timeout",
            "client_max_body_size",
            "sorry_page",
            "proxy_buffer_size",
        )

    # pylint: disable=W0613
    def search(self, queryset, name, value):
        """override"""
        if not value.strip():
            return queryset
        return queryset.filter(
            Q(source__icontains=value)
            | Q(target__icontains=value)
            | Q(Comment__icontains=value)
        )


class HttpHeaderFilterSet(NetBoxModelFilterSet):
    """HTTP Header filterset definition class"""

    class Meta:
        """Meta class"""

        model = HttpHeader
        fields = ("id", "name", "value", "apply_to", "mapping")

    # pylint: disable=W0613
    def search(self, queryset, name, value):
        """override"""
        if not value.strip():
            return queryset
        return queryset.filter(Q(name__icontains=value) | Q(value__icontains=value))

class RedirectFilterSet(NetBoxModelFilterSet):
    """Redirect filterset definition class"""

    class Meta:
        """Meta class"""
        model = Redirect
        fields = ('id', 'source', 'target', 'redirect_type', 'activation_date', 'deactivation_date', 'mapping')


# ACL DENIED SOURCE
class AclDeniedSourceFilterSet(NetBoxModelFilterSet):
    """ACL Denied Source filterset definition class"""

    # INFO : Inet type is not compatible with filterset
    acl_source = InetAddressField()
    extra_protocols = filters.CharFilter(lookup_expr="icontains")

    class Meta:
        """Meta class"""

        model = AclDeniedSource
        fields = (
            "id",
            "mapping",
            # "acl_source",
            "last_updated",
            "created",
        )

    # pylint: disable=W0613
    def search(self, queryset, name, value):
        """override"""
        if not value.strip():
            return queryset
        return queryset.filter(
            Q(id__icontains=value)
            | Q(mapping__icontains=value)

        )


class CacheConfigFilterSet(NetBoxModelFilterSet):
    """Cache configuration filterset definition class"""

    class Meta:
        """Meta class"""

        model = CacheConfig
        fields = (
            "mapping",
            "ttl",
            "max_size_limit",
        )

    # pylint: disable=W0613
    def search(self, queryset, name, value):
        """override"""
        if not value.strip():
            return queryset
        return queryset.filter(Q(ttl__icontains=value) | Q(max_size_limit__icontains=value))

class HstsProtocolFilterSet(NetBoxModelFilterSet):
    """Hsts protocol filterset definition class"""

    class Meta:
        """Meta class"""

        model = HstsProtocol
        fields = (
            "mapping",
            "subdomains",
        )

    # pylint: disable=W0613
    def search(self, queryset, name, value):
        """override"""
        if not value.strip():
            return queryset
        return queryset.filter(Q(subdomains__icontains=value))
