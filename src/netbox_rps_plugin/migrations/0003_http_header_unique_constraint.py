"""Migration File"""
# pylint: disable=C0103

from django.db import migrations, models


class Migration(migrations.Migration):
    """Migration Class"""

    dependencies = [("netbox_rps_plugin", "0002_http_header")]

    operations = [
        migrations.AlterField(
            model_name="httpheader",
            name="value",
            field=models.CharField(max_length=256, null=True, blank=True),
        ),
        migrations.AddConstraint(
            model_name="httpheader",
            constraint=models.UniqueConstraint(
                fields=("mapping", "name", "apply_to"),
                name="netbox_rps_plugin_httpheader_unique_mapping_name_apply_to",
            ),
        ),
    ]
