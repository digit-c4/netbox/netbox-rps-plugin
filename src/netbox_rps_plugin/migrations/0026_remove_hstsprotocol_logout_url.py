from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('netbox_rps_plugin', '0025_remove_hstsprotocol_logout_url_and_more'),
    ]

    operations = [
        migrations.SeparateDatabaseAndState(
            database_operations=[],
            state_operations=[
                migrations.RemoveField(
                    model_name='hstsprotocol',
                    name='logout_url',
                ),
            ]
        )
    ]
