"""Migration File"""
# pylint: disable=C0103

from django.db import migrations, models


class Migration(migrations.Migration):
    """Migration Class"""

    dependencies = [("netbox_rps_plugin", "0005_source_mapping_unique_constraint")]

    operations = [
        migrations.AlterField(
            model_name="mapping",
            name="source",
            field=models.CharField(
                null=False, blank=False, max_length=2000, unique=True
            ),
        ),
        migrations.AlterField(
            model_name="mapping",
            name="target",
            field=models.CharField(null=False, blank=False, max_length=2000),
        ),
        migrations.AlterField(
            model_name="mapping",
            name="testingpage",
            field=models.CharField(null=True, blank=True, max_length=2000),
        ),
    ]
