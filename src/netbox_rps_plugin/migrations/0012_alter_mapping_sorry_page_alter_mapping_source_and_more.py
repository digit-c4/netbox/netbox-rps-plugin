"""Migration File"""
# pylint: disable=C0103

import django.core.validators
from django.db import migrations, models
import netbox_rps_plugin.models


class Migration(migrations.Migration):
    """Migration Class"""

    dependencies = [
        ("netbox_rps_plugin", "0011_alter_mapping_extra_protocols_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="mapping",
            name="sorry_page",
            field=models.URLField(
                default="https://sorry.ec.europa.eu/",
                max_length=2000,
                validators=[
                    django.core.validators.URLValidator(schemes=["http", "https"])
                ],
            ),
        ),
        migrations.AlterField(
            model_name="mapping",
            name="source",
            field=netbox_rps_plugin.models.FilteredURLField(
                max_length=2000,
                unique=True,
                validators=[
                    django.core.validators.URLValidator(schemes=["http", "https"])
                ],
            ),
        ),
        migrations.AlterField(
            model_name="mapping",
            name="target",
            field=netbox_rps_plugin.models.FilteredURLField(
                max_length=2000,
                validators=[
                    django.core.validators.URLValidator(schemes=["http", "https"])
                ],
            ),
        ),
        migrations.AlterField(
            model_name="mapping",
            name="testingpage",
            field=models.URLField(
                blank=True,
                max_length=2000,
                null=True,
                validators=[
                    django.core.validators.URLValidator(schemes=["http", "https"])
                ],
            ),
        ),
    ]
