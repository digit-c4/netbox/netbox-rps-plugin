"""Migration File"""
# pylint: disable=C0103

import django.contrib.postgres.fields
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import netbox_rps_plugin.models


class Migration(migrations.Migration):
    """Migration Class"""

    dependencies = [
        ("netbox_rps_plugin", "0009_sorry_page"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="httpheader",
            options={"ordering": ["name"]},
        ),
        migrations.AlterModelOptions(
            name="mapping",
            options={"ordering": ("source", "target")},
        ),
        migrations.RemoveConstraint(
            model_name="httpheader",
            name="netbox_rps_plugin_httpheader_unique_mapping_name_apply_to",
        ),
        migrations.AddField(
            model_name="mapping",
            name="extra_protocols",
            field=django.contrib.postgres.fields.ArrayField(
                base_field=models.CharField(max_length=32),
                default=netbox_rps_plugin.models.default_protocol,
                size=None,
            ),
        ),
        migrations.AlterField(
            model_name="httpheader",
            name="apply_to",
            field=models.CharField(default="request", max_length=30),
        ),
        migrations.AlterField(
            model_name="httpheader",
            name="name",
            field=models.CharField(max_length=120),
        ),
        migrations.AlterField(
            model_name="mapping",
            name="authentication",
            field=models.CharField(default="none", max_length=30),
        ),
        migrations.AlterField(
            model_name="mapping",
            name="client_max_body_size",
            field=models.IntegerField(
                default=1,
                validators=[
                    django.core.validators.MinValueValidator(1),
                    django.core.validators.MaxValueValidator(255),
                ],
            ),
        ),
        migrations.AlterField(
            model_name="mapping",
            name="keepalive_requests",
            field=models.IntegerField(
                default=1000,
                validators=[
                    django.core.validators.MinValueValidator(100),
                    django.core.validators.MaxValueValidator(5000),
                ],
            ),
        ),
        migrations.AlterField(
            model_name="mapping",
            name="keepalive_timeout",
            field=models.IntegerField(
                default=75,
                validators=[
                    django.core.validators.MinValueValidator(1),
                    django.core.validators.MaxValueValidator(300),
                ],
            ),
        ),
        migrations.AlterField(
            model_name="mapping",
            name="proxy_read_timeout",
            field=models.IntegerField(
                default=60,
                validators=[
                    django.core.validators.MinValueValidator(1),
                    django.core.validators.MaxValueValidator(300),
                ],
            ),
        ),
        migrations.AlterField(
            model_name="mapping",
            name="source",
            field=models.CharField(
                max_length=2000,
                unique=True,
                validators=[
                    django.core.validators.URLValidator(message="It must be a url")
                ],
            ),
        ),
        migrations.AlterField(
            model_name="mapping",
            name="target",
            field=models.CharField(
                max_length=2000,
                validators=[
                    django.core.validators.URLValidator(message="It must be a url")
                ],
            ),
        ),
        migrations.AlterField(
            model_name="mapping",
            name="testingpage",
            field=models.CharField(
                blank=True,
                max_length=2000,
                null=True,
                validators=[
                    django.core.validators.URLValidator(message="It must be a url")
                ],
            ),
        ),
        migrations.AlterField(
            model_name="samlconfig",
            name="acs_url",
            field=models.CharField(
                max_length=2000,
                validators=[
                    django.core.validators.URLValidator(message="It must be a url")
                ],
            ),
        ),
        migrations.AlterField(
            model_name="samlconfig",
            name="logout_url",
            field=models.CharField(
                max_length=2000,
                validators=[
                    django.core.validators.URLValidator(message="It must be a url")
                ],
            ),
        ),
        migrations.AlterField(
            model_name="samlconfig",
            name="mapping",
            field=models.OneToOneField(
                db_column="mapping_id",
                on_delete=django.db.models.deletion.CASCADE,
                related_name="saml_config",
                to="netbox_rps_plugin.mapping",
            ),
        ),
        migrations.AlterUniqueTogether(
            name="httpheader",
            unique_together={("mapping", "name", "apply_to")},
        ),
    ]
