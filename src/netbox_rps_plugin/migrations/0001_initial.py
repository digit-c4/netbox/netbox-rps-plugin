"""Migration File"""
# pylint: disable=C0103

from django.db import migrations, models
import taggit.managers
import utilities.json # pylint: disable=import-error


class Migration(migrations.Migration):
    """Migration Class"""

    initial = True

    dependencies = [
        ("extras", "0084_staging"),
        ("ipam", "0063_standardize_description_comments"),
    ]

    operations = [
        migrations.CreateModel(
            name="Mapping",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True, primary_key=True, serialize=False
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True, null=True)),
                ("last_updated", models.DateTimeField(auto_now=True, null=True)),
                (
                    "custom_field_data",
                    models.JSONField(
                        blank=True,
                        default=dict,
                        encoder=utilities.json.CustomFieldJSONEncoder,
                    ),
                ),
                ("source", models.CharField(blank=True, max_length=120)),
                ("target", models.CharField(blank=True, max_length=120)),
                ("authentication", models.CharField(max_length=30)),
                ("testingpage", models.CharField(max_length=120)),
                ("webdav", models.BooleanField(default=False)),
                ("Comment", models.CharField(blank=True, max_length=500)),
                (
                    "tags",
                    taggit.managers.TaggableManager(
                        through="extras.TaggedItem", to="extras.Tag"
                    ),
                ),
            ],
            options={
                "ordering": ("source"),
            },
        ),
    ]
