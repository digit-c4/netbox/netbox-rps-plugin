"""Search definitions"""

from netbox.search import SearchIndex, register_search # pylint: disable=import-error
from .models import Mapping, HttpHeader


@register_search
class MappingIndex(SearchIndex):
    """Mapping search definition class"""

    model = Mapping
    fields = (
        ("source", 120),
        ("target", 120),
        ("authentication", 30),
        ("Comment", 500),
    )


@register_search
class HttpHeaderIndex(SearchIndex):
    """Mapping search definition class"""

    model = HttpHeader
    fields = (("name", 120), ("value", 256), ("apply_to", 8))
