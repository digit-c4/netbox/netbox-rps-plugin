"""Forms definitions"""

from django import forms  # pylint: disable=import-error
from django.utils.translation import gettext as _  # pylint: disable=import-error
from netbox.forms import (  # pylint: disable=import-error, no-name-in-module
    NetBoxModelForm,  # pylint: disable=import-error, no-name-in-module
    NetBoxModelFilterSetForm,  # pylint: disable=import-error, no-name-in-module
    NetBoxModelImportForm,  # pylint: disable=import-error, no-name-in-module
)  # pylint: disable=import-error, no-name-in-module
from utilities.forms.fields import (
    DynamicModelMultipleChoiceField,
    TagFilterField,
)  # pylint: disable=import-error, wrong-import-order
from utilities.forms import (
    BOOLEAN_WITH_BLANK_CHOICES,
    add_blank_choice,
)  # pylint: disable=import-error, wrong-import-order
from netfields import InetAddressField
from .models import (  # pylint: disable=syntax-error
    Mapping,
    AuthenticationChoices,
    ExtraProtocolChoices,
    ExtensionChoices,
    HttpHeader,
    ApplyToChoices,
    SamlConfig,
    CacheConfig,
    HstsProtocol,
    URL_MAX_SIZE,
    AclDeniedSource,
    Redirect,
    RedirectTypeChoices,
)

# Section : Static data


class MappingForm(NetBoxModelForm):
    """Mapping form definition class"""

    extra_protocols = forms.MultipleChoiceField(
        choices=ExtraProtocolChoices, required=False
    )

    class Meta:
        """Meta class"""

        model = Mapping
        fields = (
            "source",
            "target",
            "authentication",
            "webdav",
            "extra_protocols",
            "testingpage",
            "gzip_proxied",
            "keepalive_requests",
            "keepalive_timeout",
            "proxy_cache",
            "proxy_read_timeout",
            "client_max_body_size",
            "sorry_page",
            "proxy_buffer_size",
            "proxy_buffer",
            "proxy_busy_buffer",
            "Comment",
            "tags",
            "proxy_buffer_responses",
            "proxy_buffer_requests",
        )

        help_texts = {
            "target": "URL-friendly unique shorthand",
            "keepalive_requests": "Min value 100 requests, max 5000 requests, "
            + "default 1000 requests.",
            "keepalive_timeout": "In seconds. Min value 1, max 300, default 75.",
            "proxy_read_timeout": "In seconds. Min value 1, max 300, default 60.",
            "client_max_body_size": "In Mega Bytes. Choose between 128, 512, 1024 or 2048",
            "proxy_buffer_size": "In Kilo Bytes, min value 4, max value 128 - possible values: 4 kB, 32 kB, 64 kB, 128 kB",
            "proxy_buffer": "In Kilo Bytes, min value 16, max value 512 - Should be 4x Proxy Buffer Size",
            "proxy_busy_buffer": "In Kilo Bytes, min value 8, max value 256 - Should be 2x Proxy Buffer Size",
            "proxy_buffer_responses": "Enable/Disable buffering of requests received by the Proxy, from the Client, before sending them to the Upstream",
            "proxy_buffer_requests": "Enable/Disable buffering of responses received by the Proxy, from the Upstream, before sending them to the Client",
        }
        labels = {
            "source": "Source URL",
            "target": "Target URL",
            "testingpage": "Testing Page Endpoint",
            "keepalive_timeout": "Keepalive timeout (s)",
            "proxy_read_timeout": " Proxy read timeout (s)",
            "client_max_body_size": "Client max body size (MB)",
            "sorry_page": "Sorry Page URL",
            "protocols": "Protocols",
            "proxy_buffer_size": "Proxy Buffer Size",
            "proxy_buffer": "Proxy Buffer",
            "proxy_busy_buffer": "Proxy Busy Buffer",
            "proxy_buffer_responses": "Buffering of responses from upstream",
            "proxy_buffer_requests": "Buffering of requests to upstream",
        }


class MappingFilterForm(NetBoxModelFilterSetForm):
    """Mapping filter form definition class"""

    model = Mapping
    source = forms.CharField(
        max_length=URL_MAX_SIZE, min_length=1, required=False, label="Source URL"
    )
    target = forms.CharField(
        max_length=URL_MAX_SIZE, min_length=1, required=False, label="Target URL"
    )
    authentication = forms.MultipleChoiceField(
        choices=AuthenticationChoices,
        required=False,
    )
    webdav = forms.BooleanField(
        required=False, widget=forms.Select(choices=BOOLEAN_WITH_BLANK_CHOICES)
    )
    extra_protocols = forms.MultipleChoiceField(
        choices=ExtraProtocolChoices,
        required=False,
    )
    testingpage = forms.CharField(
        max_length=120, min_length=1, required=False, label="Testing URL"
    )
    gzip_proxied = webdav = forms.BooleanField(
        required=False, widget=forms.Select(choices=BOOLEAN_WITH_BLANK_CHOICES)
    )
    keepalive_requests = forms.IntegerField(
        min_value=100, max_value=5000, required=False
    )
    keepalive_timeout = forms.IntegerField(min_value=1, max_value=300, required=False)
    proxy_cache = forms.BooleanField(
        required=False, widget=forms.Select(choices=BOOLEAN_WITH_BLANK_CHOICES)
    )
    proxy_read_timeout = forms.IntegerField(min_value=1, max_value=300, required=False)
    client_max_body_size = forms.IntegerField(required=False)
    sorry_page = forms.CharField(
        max_length=URL_MAX_SIZE, min_length=1, required=False, label="Sorry Page URL"
    )
    proxy_buffer_size = forms.IntegerField(
        min_value=4,
        max_value=128,
        required=False,
    )
    proxy_buffer = forms.IntegerField(
        min_value=16,
        max_value=512,
        required=False,
    )
    proxy_busy_buffer = forms.IntegerField(
        min_value=8,
        max_value=256,
        required=False,
    )

    Comment = forms.CharField(
        max_length=500, min_length=1, required=False, label="Comment"
    )
    tag = TagFilterField(model)


class MappingImportForm(NetBoxModelImportForm):
    """Mapping importation form definition class"""

    authentication = forms.MultipleChoiceField(
        choices=AuthenticationChoices,
        required=False,
        help_text='Authentication method. Can be "none", "ldap" and "ecas". Default to "none".',
    )
    keepalive_requests = forms.IntegerField(
        min_value=100,
        max_value=5000,
        required=False,
        help_text="Min value 100 requests, max 5000 requests, default 1000 requests.",
    )
    keepalive_timeout = forms.IntegerField(
        min_value=1,
        max_value=300,
        required=False,
        help_text="In seconds. Min value 1, max 300, default 75.",
    )
    proxy_read_timeout = forms.IntegerField(
        min_value=1,
        max_value=300,
        required=False,
        help_text="In seconds. Min value 1, max 300, default 60.",
    )
    client_max_body_size = forms.IntegerField(
        required=False,
        help_text="In Mega Bytes. Choose between 128, 512, 1024 or 2048",
    )
    sorry_page = forms.CharField(
        max_length=URL_MAX_SIZE,
        min_length=1,
        required=False,
        help_text="Sorry Page URL",
    )
    proxy_buffer_size = forms.IntegerField(
        min_value=4,
        max_value=128,
        required=False,
    )
    proxy_buffer = forms.IntegerField(
        min_value=16,
        max_value=512,
        required=False,
    )
    proxy_busy_buffer = forms.IntegerField(
        min_value=8,
        max_value=256,
        required=False,
    )

    class Meta:
        """Meta class"""

        model = Mapping
        fields = (
            "source",
            "target",
            "authentication",
            "testingpage",
            "webdav",
            "testingpage",
            "Comment",
            "gzip_proxied",
            "keepalive_requests",
            "keepalive_timeout",
            "proxy_cache",
            "proxy_read_timeout",
            "client_max_body_size",
            "sorry_page",
        )
        help_texts = {
            "source": "Source URL",
            "target": "Target URL",
            "testingpage": "Testing page URL",
            "webdav": 'Define if the mapping handle Webdav protocol. Default to "false"',
            "gzip_proxied": 'Is gzip proxied. Default to "false"',
            "proxy_cache": 'Is proxy cache activated. Default to "false"',
        }


class HttpHeaderForm(NetBoxModelForm):
    """HTTP header form definition class"""

    class Meta:
        """Meta class"""

        model = HttpHeader
        fields = ("mapping", "name", "value", "apply_to")
        labels = {
            "mapping": "Mapping",
            "name": "Name",
            "value": "Value",
            "apply_to": "Apply to",
        }


class HttpHeaderFilterForm(NetBoxModelFilterSetForm):
    """HTTP header filter form definition class"""

    model = HttpHeader
    name = forms.CharField(
        max_length=120, min_length=1, required=False, label="Header name"
    )
    value = forms.CharField(
        max_length=256, min_length=1, required=False, label="Header value"
    )
    apply_to = forms.ChoiceField(
        choices=add_blank_choice(ApplyToChoices), required=False, label="Apply to"
    )
    mapping = DynamicModelMultipleChoiceField(
        queryset=Mapping.objects.all(), required=False, label=_("Mapping")
    )
    tag = TagFilterField(model)


class RedirectForm(NetBoxModelForm):
    """ Redirect form definition class"""

    class Meta:
        """Meta class"""

        model = Redirect
        fields = (
            'mapping',
            'source',
            'target',
            'redirect_type',
            'activation_date',
            'deactivation_date',
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['mapping'].queryset = Mapping.objects.all()

class RedirectFilterForm(NetBoxModelFilterSetForm):
    """Redirect filter form definition class"""

    model = Redirect
    source = forms.CharField(
        max_length=2000, min_length=1, required=False, label="Source"
    )
    target = forms.CharField(
        max_length=2000, min_length=1, required=False, label="Target"
    )
    redirect_type = forms.ChoiceField(
        choices=add_blank_choice(RedirectTypeChoices), required=False, label="Redirect type"
    )
    mapping = DynamicModelMultipleChoiceField(
        queryset=Mapping.objects.all(), required=False, label=_("Mapping")
    )
    activation_date = forms.CharField(required=False, label="Activation date")
    deactivation_date = forms.CharField(required=False, label="Deactivation date")
    tag = TagFilterField(model)


class SamlConfigForm(NetBoxModelForm):
    """SAML config form definition class"""

    class Meta:
        """Meta class"""

        model = SamlConfig
        fields = ("mapping", "acs_url", "logout_url", "force_nauth")
        labels = {
            "mapping": "Mapping",
            "acs_url": "ACS URL",
            "logout_url": "Logout URL",
            "force_nauth": "Force AuthnRequest",
        }


# ACL DENIED FORM
class AclDeniedSourceForm(NetBoxModelForm):
    """Acl Denied Source form definition class"""

    # Info : This code is no-longer relevant if you want to include IPV6 as well.
    # REGEX_IPV4=r"((25[0-5]|2[0-4][0-9]|[01]?[0-9]?[0-9])\.){3}" \
    # "(25[0-5]|2[0-4][0-9]|[01]?[0-9]?[0-9])"
    # acl_source = forms.CharField(validators=[RegexValidator(regex=rf"^{REGEX_IPV4}\/[123]?[0-9]$)])
    acl_source = InetAddressField()

    class Meta:
        """Meta class"""

        model = AclDeniedSource
        fields = (
            "mapping",
            "acl_source",
        )
        help_texts = {
            "acl_source": "IP address (IPv4 or IPv6) and the subnet (CIDR notation)",
            "mapping": "Source related to the target mapping",
        }
        labels = {
            "mapping": "Mapping",
            "acl_source": "ACL list record",
        }


class AclDeniedSourceFilterForm(NetBoxModelFilterSetForm):
    """AclDeniedSource filter form definition class"""

    model = AclDeniedSource
    mapping = forms.IntegerField(required=True, label="Mapping")
    acl_source = forms.CharField(required=True, label="ACL list record")


class CacheConfigForm(NetBoxModelForm):
    """Cache config form definition class"""

    list_extensions = forms.MultipleChoiceField(
        required=False, choices=ExtensionChoices
    )

    class Meta:
        """Meta class"""

        model = CacheConfig
        fields = (
            "mapping",
            "list_extensions",
            "ttl",
            "max_size_limit",
        )
        help_texts = {
            "mapping": "",
            "list_extensions": "Used Extension (JPG, GIF, CSS, HTML, JS, XML, Other)",
            "ttl": "In seconds",
            "max_size_limit": "In Mega Bytes. Min 1, Max 100, default 100.",
        }
        labels = {
            "mapping": "Related Mapping",
            "list_extensions": "Extension",
            "ttl": "TTL",
            "max_size_limit": "Max Size",
        }


class CacheConfigFilterForm(NetBoxModelFilterSetForm):
    """Cache config filter form definition class"""

    model = CacheConfig
    mapping = DynamicModelMultipleChoiceField(
        queryset=Mapping.objects.all(), required=False, label=_("Mapping")
    )
    list_extensions = forms.MultipleChoiceField(
        required=False, choices=ExtensionChoices
    )
    ttl = forms.IntegerField(min_value=1)
    max_size_limit = forms.IntegerField()


class HstsProtocolForm(NetBoxModelForm):
    """Hsts Protocol from definition class"""

    class Meta:
        """Meta class"""

        model = HstsProtocol
        fields = (
            "mapping",
            "subdomains",
            "preload_flag",
            "max_age",
        )
        help_texts = {
            "mapping": "",
            "subdomains": "includeSubDomains directive",
            "preload_flag": "To prevent the first connection risk",
            "max_age": "HSTS max-age property (0, 1 or 2 years converted in seconds)",
        }
        labels = {
            "mapping": "Mapping",
            "subdomains": "SubDomains",
            "preload_flag": "Preload Flag",
            "max_age": "Max Age",
        }


class HstsProtocolFilterForm(NetBoxModelFilterSetForm):
    """Hsts Procotol filter from definition class"""

    model = HstsProtocol
    mapping = DynamicModelMultipleChoiceField(
        queryset=Mapping.objects.all(), required=True, label=_("Mapping")
    )

    subdomains = forms.BooleanField()
    preload_flag = forms.BooleanField()
    max_age = forms.IntegerField()
    logout_url = forms.CharField(max_length=2000)
