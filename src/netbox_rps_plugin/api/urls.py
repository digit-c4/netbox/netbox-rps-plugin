"""API URLs definition"""

from netbox.api.routers import NetBoxRouter # pylint: disable=import-error
from . import views

APP_NAME = 'netbox_rps_plugin'

router = NetBoxRouter()
router.register('mapping', views.MappingViewSet)
router.register('http_header', views.HttpHeaderViewSet)
router.register('saml_config', views.SamlConfigViewSet)
router.register('acldeniedsource', views.AclDeniedSourceViewSet)
router.register('cache_config', views.CacheConfigViewSet)
router.register('hsts_protocol', views.HstsProtocolViewSet)
router.register('redirect', views.RedirectViewSet)

urlpatterns = router.urls
