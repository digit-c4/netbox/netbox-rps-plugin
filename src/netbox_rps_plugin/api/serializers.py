"""API Serializer definitions"""

from rest_framework import serializers  # pylint: disable=import-error
from netbox.api.serializers import (
    NetBoxModelSerializer,
    WritableNestedSerializer,
)  # pylint: disable=import-error
from ..models import (
    Mapping,
    HttpHeader,
    SamlConfig,
    HstsProtocol,
    clean_url,
    AclDeniedSource,
    CacheConfig,
    Redirect,
)


class NestedMappingSerializer(WritableNestedSerializer):
    """Nested Mapping Serializer class"""

    url = serializers.HyperlinkedIdentityField(
        view_name="plugins-api:netbox_rps_plugin-api:mapping-detail"
    )

    class Meta:
        """Meta Class"""

        model = Mapping
        fields = ("id", "url", "display")


class NestedSamlConfigSerializer(WritableNestedSerializer):
    """Nested SAML Config Serializer class"""

    url = url = serializers.HyperlinkedIdentityField(
        view_name="plugins-api:netbox_rps_plugin-api:samlconfig-detail"
    )

    class Meta:
        """Meta Class"""

        model = SamlConfig
        fields = (
            "id",
            "url",
            "acs_url",
            "logout_url",
            "force_nauth",
        )


class SamlConfigSerializer(NetBoxModelSerializer):
    """SAML Config Serializer class"""

    url = serializers.HyperlinkedIdentityField(
        view_name="plugins-api:netbox_rps_plugin-api:samlconfig-detail"
    )

    mapping = NestedMappingSerializer()

    class Meta:
        """Meta Class"""

        model = SamlConfig
        fields = (
            "id",
            "url",
            "acs_url",
            "logout_url",
            "force_nauth",
            "mapping",
            "custom_fields",
            "created",
            "last_updated",
            "tags",
        )


class HttpHeaderSerializer(NetBoxModelSerializer):
    """HTTP Header Serializer class"""

    url = serializers.HyperlinkedIdentityField(
        view_name="plugins-api:netbox_rps_plugin-api:httpheader-detail"
    )

    mapping = NestedMappingSerializer()

    class Meta:
        """Meta Class"""

        model = HttpHeader
        fields = (
            "id",
            "url",
            "name",
            "value",
            "apply_to",
            "mapping",
            "custom_fields",
            "created",
            "last_updated",
            "tags",
        )


class NestedHttpHeaderSerializer(WritableNestedSerializer):
    """Nested HTTP Header Serializer class"""

    url = serializers.HyperlinkedIdentityField(
        view_name="plugins-api:netbox_rps_plugin-api:httpheader-detail"
    )

    class Meta:
        """Meta Class"""

        model = HttpHeader
        fields = ("id", "url", "name", "value", "apply_to")


class RedirectSerializer(serializers.ModelSerializer):
    """Redirect Serializer class"""

    mapping = NestedMappingSerializer()

    class Meta:
        """Meta Class"""
        model = Redirect
        fields = (
            'id',
            'source',
            'target',
            'mapping',
            'redirect_type',
            'activation_date',
            'deactivation_date',
            'created',
            'last_updated'
        )

    def validate(self, attrs):
        """Be sure that all is cleaned"""
        # For partial updates, get the existing instance
        instance = self.instance if self.instance else Redirect(**attrs)
        for attr, value in attrs.items():
            setattr(instance, attr, value)
        instance.full_clean()
        return attrs


class NestedRedirectSerializer(WritableNestedSerializer):
    """Nested Redirect Serializer class"""

    url = serializers.HyperlinkedIdentityField(
        view_name="plugins-api:netbox_rps_plugin-api:redirect-detail"
    )

    # mapping = NestedMappingSerializer()
    mapping = serializers.PrimaryKeyRelatedField(queryset=Mapping.objects.all())

    class Meta:
        """Meta Class"""

        model = Redirect
        fields = ("id", "url", "source", "target", "redirect_type", "mapping", "activation_date", "deactivation_date")


class NestedAclDeniedSourceSerializer(WritableNestedSerializer):
    """Nested HTTP Header Serializer class"""

    url = serializers.HyperlinkedIdentityField(
        view_name="plugins-api:netbox_rps_plugin-api:acldeniedsource-detail"
    )

    class Meta:
        """Meta Class"""

        model = AclDeniedSource
        fields = ("id", "url", "mapping", "acl_source")


class CacheConfigSerializer(NetBoxModelSerializer):
    """Cache Configuration Serializer class"""

    url = serializers.HyperlinkedIdentityField(
        view_name="plugins-api:netbox_rps_plugin-api:cacheconfig-detail"
    )

    mapping = NestedMappingSerializer()

    class Meta:
        """Meta Class"""

        model = CacheConfig
        fields = (
            "id",
            "url",
            "mapping",
            "list_extensions",
            "ttl",
            "max_size_limit",
            "last_updated",
            "created",
        )


class NestedCacheConfigSerializer(WritableNestedSerializer):
    """Nested Cache Config Serializer class"""

    url = serializers.HyperlinkedIdentityField(
        view_name="plugins-api:netbox_rps_plugin-api:cacheconfig-detail"
    )

    class Meta:
        """Meta Class"""

        model = CacheConfig
        fields = (
            "id",
            "url",
            "mapping",
            "list_extensions",
            "ttl",
            "max_size_limit",
            "last_updated",
            "created",
        )


class HstsProtocolSerializer(NetBoxModelSerializer):
    """Hsts Protocol Serializer class"""

    url = serializers.HyperlinkedIdentityField(
        view_name="plugins-api:netbox_rps_plugin-api:hstsprotocol-detail"
    )

    mapping = NestedMappingSerializer()

    class Meta:
        """Meta Class"""

        model = HstsProtocol
        fields = (
            "id",
            "url",
            "mapping",
            "subdomains",
            "preload_flag",
            "max_age",
            "last_updated",
        )


class NestedHstsProtocolSerializer(WritableNestedSerializer):
    """Nested Hsts Protocol Serializer class"""

    url = serializers.HyperlinkedIdentityField(
        view_name="plugins-api:netbox_rps_plugin-api:hstsprotocol-detail"
    )

    class Meta:
        """Meta Class"""

        model = HstsProtocol
        fields = (
            "id",
            "url",
            "mapping",
            "subdomains",
            "preload_flag",
            "max_age",
            "last_updated",
        )


class MappingSerializer(NetBoxModelSerializer):
    """Mapping Serializer class"""

    url = serializers.HyperlinkedIdentityField(
        view_name="plugins-api:netbox_rps_plugin-api:mapping-detail"
    )

    http_headers = NestedHttpHeaderSerializer(many=True, required=False)
    saml_config = NestedSamlConfigSerializer(required=False, allow_null=True)
    acl_denied_source = NestedAclDeniedSourceSerializer(many=True, read_only=True)
    cache_configs = NestedCacheConfigSerializer(many=True, read_only=True)
    hsts_protocol = NestedHstsProtocolSerializer(allow_null=True, read_only=True)
    redirects = NestedRedirectSerializer(many=True, allow_null=True, read_only=True)

    class Meta:
        """Meta Class"""

        model = Mapping
        fields = (
            "id",
            "url",
            "source",
            "target",
            "authentication",
            "testingpage",
            "webdav",
            "Comment",
            "gzip_proxied",
            "keepalive_requests",
            "keepalive_timeout",
            "proxy_cache",
            "proxy_read_timeout",
            "client_max_body_size",
            "extra_protocols",
            "sorry_page",
            "custom_fields",
            "created",
            "last_updated",
            "tags",
            "http_headers",
            "saml_config",
            "proxy_buffer_size",
            "proxy_buffer",
            "proxy_busy_buffer",
            "acl_denied_source",
            "cache_configs",
            "hsts_protocol",
            "redirects",
            "proxy_buffer_responses",
            "proxy_buffer_requests",
        )

    def create(self, validated_data):
        """Be sure that URL is cleaned"""

        source = validated_data.pop("source", None)
        target = validated_data.pop("target", None)

        if source is not None:
            validated_data["source"] = clean_url(source)

        if target is not None:
            validated_data["target"] = clean_url(target)

        return super().create(validated_data)

    def update(self, instance, validated_data):
        """Be sure that URL is cleaned"""

        validated_data["source"] = clean_url(validated_data["source"])
        validated_data["target"] = clean_url(validated_data["target"])

        return super().update(instance, validated_data)


class AclDeniedSourceSerializer(NetBoxModelSerializer):
    """AclDeniedSource Serializer class"""

    url = serializers.HyperlinkedIdentityField(
        view_name="plugins-api:netbox_rps_plugin-api:acldeniedsource-detail"
    )

    mapping = NestedMappingSerializer()

    class Meta:
        """Meta Class"""

        model = AclDeniedSource
        fields = (
            "id",
            "url",
            "mapping",
            "acl_source",
            "last_updated",
            "created",
        )
