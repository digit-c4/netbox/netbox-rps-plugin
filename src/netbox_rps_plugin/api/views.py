"""API views definitions"""

from netbox.api.viewsets import NetBoxModelViewSet  # pylint: disable=import-error
from .. import filtersets, models
from .serializers import (
    MappingSerializer,
    HttpHeaderSerializer,
    SamlConfigSerializer,
    HstsProtocolSerializer,
    AclDeniedSourceSerializer,
    CacheConfigSerializer,
    RedirectSerializer
)


class MappingViewSet(NetBoxModelViewSet):
    """Mapping view set class"""

    queryset = models.Mapping.objects.prefetch_related("http_headers", "tags").all()
    serializer_class = MappingSerializer
    filterset_class = filtersets.MappingFilterSet
    http_method_names = ["get", "post", "patch", "delete", "options"]


class HttpHeaderViewSet(NetBoxModelViewSet):
    """HTTP Header view set class"""

    queryset = models.HttpHeader.objects.prefetch_related("mapping", "tags").all()
    serializer_class = HttpHeaderSerializer
    filterset_class = filtersets.HttpHeaderFilterSet
    http_method_names = ["get", "post", "patch", "delete", "options"]


class RedirectViewSet(NetBoxModelViewSet):
    """Redirect view set class"""

    queryset = models.Redirect.objects.prefetch_related("mapping", "tags").all()
    serializer_class = RedirectSerializer
    filterset_class = filtersets.RedirectFilterSet
    http_method_names = ["get", "post", "patch", "delete", "options"]


class SamlConfigViewSet(NetBoxModelViewSet):
    """SAML config view set class"""

    queryset = models.SamlConfig.objects.prefetch_related("mapping", "tags").all()
    serializer_class = SamlConfigSerializer
    http_method_names = ["get", "post", "patch", "delete", "options"]


class AclDeniedSourceViewSet(NetBoxModelViewSet):
    """ACL Denied Source view set class"""

    queryset = models.AclDeniedSource.objects.prefetch_related("mapping", "tags").all()
    serializer_class = AclDeniedSourceSerializer
    filterset_class = filtersets.AclDeniedSourceFilterSet
    http_method_names = ["get", "post", "patch", "delete", "options"]


class CacheConfigViewSet(NetBoxModelViewSet):
    """Cache config view set class"""

    queryset = models.CacheConfig.objects.prefetch_related("mapping", "tags").all()
    serializer_class = CacheConfigSerializer
    filterset_class = filtersets.CacheConfigFilterSet
    http_method_names = ["get", "post", "patch", "delete", "options"]


class HstsProtocolViewSet(NetBoxModelViewSet):
    """Hsts Protocol view set class"""

    queryset = models.HstsProtocol.objects.prefetch_related("mapping", "tags").all()
    serializer_class = HstsProtocolSerializer
    filterset_class = filtersets.HstsProtocolFilterSet
    http_method_names = ["get", "post", "patch", "delete", "options"]
