# RPS Plugin for Netbox

A Netbox plugin for RPS mapping management.

![Screenshot of the Netbox RPS plugin detail screen](./docs/screenshot.png)

## Contribute

### Install our development environment
> Requirements:
> * Python 3.11 & Poetry (See [Python best practices](https://code.europa.eu/digit-c4/dev/python-best-practices))
> * Docker
>
> For production deployment, or users without docker, the backend
> services should be installed according to [Netbox install guide](https://github.com/netbox-community/netbox/blob/master/docs/installation/index.md) and configured
> using the dev environment settings
> * PostgreSQL 15
>   * username: username (with database creation right)
>   * password: password
>   * database: netbox
>   * port: 5432
> * Redis 7.2
>   * port: 6379

Init the project according to [best practices](https://code.europa.eu/digit-c4/dev/best-practices)
```bash
PROJECT_HOME="${HOME}/Workspace/code.europa.eu/digit-c4/netbox/netbox-rps-plugin"
mkdir -p $PROJECT_HOME
git clone git@code.europa.eu:digit-c4/netbox/netbox-rps-plugin.git $PROJECT_HOME \
  && cd $PROJECT_HOME
```

Netbox core application is unfortunately not distributed (yet) in PyPi. Its code must therefore
be imported as an external library in `/lib` using git.
```shell
mkdir -p lib
git clone --branch v3.6.9 https://github.com/netbox-community/netbox.git lib/netbox
```

Init python venv according to [Python best practices](https://code.europa.eu/digit-c4/dev/python-best-practices) then install both Netbox core and this module dependencies
*(use local Python venv instead of messing up with /opt root dir as advised by official tutorial)*
```bash
python3 -m venv venv && source venv/bin/activate
pip install -r lib/netbox/requirements.txt
poetry install
```

Start a local PostgreSQL and Redis DB *(using docker instead of going through the awful local install of DBs advised by official tutorial)*
```bash
docker compose up -d
```

Configure Netbox to connect with the previously deployed database
* using standard config file
  ```
  cp netbox_configuration/configuration_dev.py lib/netbox/netbox/netbox/configuration.py
  ```
* using environment variables 
  ```bash
  cp .env.example .env
  ```
  > **Extra step when using manage.py script**
  >
  > `netbox-nms` CLI will search for environment automatically. But manage.py is not that smart. You need to tell Netbox to load configuration from environment
  > ```shell
  > export NETBOX_CONFIGURATION=netbox_nms_common.dotenv_config
  > ```

Run database migrations:

```bash
netbox-nms migrate
# Or use the Poetry-less command
python -m netbox_nms_common.manage migrate
# Or directly use the manage.py script in Netbox core
python lib/netbox/netbox/manage.py migrate
```

Create a Netbox super user:

```bash
netbox-nms createsuperuser
# Or use the Poetry-less command
python -m netbox_nms_common.manage createsuperuser
# Or directly use the manage.py script in Netbox core
python lib/netbox/netbox/manage.py createsuperuser
```

Start the Netbox instance:

```bash
netbox-nms runserver 0.0.0.0:8000 --insecure
# Or use the Poetry-less command
python -m netbox_nms_common.manage runserver 0.0.0.0:8000 --insecure
# Or directly use the manage.py script in Netbox core
python lib/netbox/netbox/manage.py runserver 0.0.0.0:8000 --insecure
```

Visit http://localhost:8000/

### Run tests

After installing you development environment, you can run the tests plugin (you
don't need to start the Netbox instance):

```bash
netbox-nms test tests --keepdb -v 2
# Or directly use the manage.py script in Netbox core
python lib/netbox/netbox/manage.py test tests --keepdb -v 2
```

If you want to recreate the testing database remove the `--keepdb` flag from the
command above.

With code coverage, install [coverage.py](https://coverage.readthedocs.io/en/7.3.2/) and use it:

```bash
poetry install --with test
```

The run the test with coverage.py and print the report:

```bash
coverage run --include='*/src/netbox_rps_plugin/*' lib/netbox/netbox/manage.py test tests -v 2
coverage report -m
```

### Generate Data Model Diagram

```bash
python3 manage.py graph_models --pydot  netbox_rps_plugin -o docs/model/schema_diagram.svg  
```
