## netbox_rps_plugin_acl_denied_source

**Acl denied source table definition**

| 	FIELD	       | 	TYPE	                 | 	DESCRIPTION	                                                        |
|---------------|------------------------|----------------------------------------------------------------------|
| id	           | 	Big (8 byte) integer	 | 	Primary key ID                                                      |
| mapping_id	   | 	Big (8 byte) integer	 | 	Refer [netbox_rps_plugin_mapping.id](netbox_rps_plugin_mapping.md)	 |
| acl_source    | Inet                   | ACL list record                                                      |
| last_updated	 | 	Date (with time)	     | 	Last updated	                                                       |
| created	      | 	Date (with time)	     | 	Created	                                                            |

