# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

### Added

- 88-acl-for-a-mapping - Created acl_denied_source [table](netbox_rps_plugin_acl_denied_source.md)
- ...

### Changed

- Updates to existing features.
- ...

### Deprecated

- Features that will be removed in future releases.
- ...

### Removed

- Deprecated features that have been removed.
- ...

### Fixed

- Bug fixes.
- ...

## [Version] - YYYY-MM-DD

### Added

- ...

### Changed

- ...

### Deprecated

- ...

### Removed

- ...

### Fixed

- ...

## [Older Versions...]

...

[Unreleased]: https://github.com/your/project/compare/vX.Y.Z...HEAD

[Version]: https://github.com/your/project/releases/tag/vX.Y.Z
