## netbox_rps_plugin_mapping

**Mapping table definition**

| 	FIELD	                | 	TYPE	                           | 	REQUIRED | Default value	 | DESCRIPTION	                                                                    |
|------------------------|----------------------------------|-----------|----------------|---------------------------------------------------------------------------------|
| 	Comment	              | 	String (up to 500)	             | Y         |                | 	Comment	                                                                       |
| 	authentication	       | 	String (up to 30)	              | Y         |                | 	Auth	                                                                          |
| 	client_max_body_size	 | 	Integer	                        | 	Client max body size. Possible values  (Mb): 1, 128, 512, 1024, 2048 |
| 	created	              | 	Date (with time)	               |           |                | 	created	                                                                       |
| 	custom_field_data	    | 	A JSON object	                  | Y         |                | 	custom field data	                                                             |
| 	extra_protocols	      | 	Array of String (up to None)	   | Y         |                | 	Extra Protocols	                                                               |
| 	gzip_proxied	         | 	Boolean (Either True or False)	 | Y         |                | 	gzip proxied	                                                                  |
| 	id	                   | 	Big (8 byte) integer	           | Y         |                | 	ID	                                                                            |
| 	keepalive_requests	   | 	Integer	                        | Y         |                | 	keepalive requests	                                                            |
| 	keepalive_timeout	    | 	Integer	                        | Y         |                | 	keepalive timeout	                                                             |
| 	last_updated	         | 	Date (with time)	               |           |                | 	last updated	                                                                  |
| 	proxy_cache	          | 	Boolean (Either True or False)	 | Y         |                | 	proxy cache	                                                                   |
| 	proxy_read_timeout	   | 	Integer	                        | Y         |                | 	proxy read timeout	                                                            |
| 	sorry_page	           | 	URL	                            | Y         |                | 	Sorry Page	                                                                    |
| 	source	               | 	URL	                            | Y         |                | 	Source	                                                                        |
| 	target	               | 	URL	                            | Y         |                | 	Target	                                                                        |
| 	testingpage	          | 	URL	                            |           |                | 	testingpage	                                                                   |
| 	webdav	               | 	Boolean (Either True or False)	 | Y         |                | 	webdav	                                                                        |
| 	proxy_buffer_size	    | 	Integer	                        | N         | 4              | 	[kB] min value 4, max value 128 - possible values: 4 kB, 32 kB, 64 kB, 128 kB	 |
| 	proxy_buffer	         | 	Integer	                        | N         | 16             | 	[kB] (4 time proxy_buffer_size), min value 16, max value 512	                  |
| 	proxy_busy_buffer	    | 	Integer	                        | N         | 8              | 	[kB] (2 time proxy_buffer_size), min value 8, max value 256	                   |
