## netbox_rps_redirect

**Redirect definition class**

| 	FIELD	               | 	TYPE	                        | 	DESCRIPTION	                                                           |
|--------------------------|------------------------------------|--------------------------------------------------------------------------|
| 	id	                   | 	Big (8 byte) integer	        | 	Unique ID	                                                                   |
| 	mapping_id             | 	Big (8 byte) integer	        | 	FK / Refer [netbox_rps_plugin_mapping.id](netbox_rps_plugin_mapping.md) / Required|
| 	source	               | 	String (up to 2000)	            |   URL of the source / Required  / Should allow all kind of characters since it will include regex expressions    |
| 	target	               | 	String (up to 2000)	            |   URL for the target / Required / Should allow all kind of characters since it will include regex expressions    |
|   redirect_type          |    Array of String (up to None)    |   List of redirect types. Values: "1-1", "N-N", "N-1" / Required|
|   activation_date	       | 	Date (with time)	            | 	Date of activation / Default: "Today"	                                                        |
| 	deactivation_date	   | 	Date (with time)	            | 	Data of deactivation / Default: null / Validation: Should always be after the "activation date"     |