## netbox_rps_plugin_cacheconfig

**Cache Configuration source table definition**

| 	FIELD	       | 	TYPE	                 | 	DESCRIPTION	                                                        |
|---------------|------------------------|----------------------------------------------------------------------|
| id	           | 	Big (8 byte) integer	 | 	Primary key ID                                                      |
| mapping_id	   | 	Big (8 byte) integer	 | 	Refer [netbox_rps_plugin_mapping.id](netbox_rps_plugin_mapping.md)	 |
| list_extensions    | Array of String (up to None)                   | List of extensions (JPG, GIF, CSS, HTML, JS, XML, Other)                                                     |
| ttl    | Integer                   | Time to live (second)                                                      |
| max_size_limit    | Integer                   | (default and max value 100 MB)                                                      |
| last_updated	 | 	Date (with time)	     | 	Last updated	                                                       |
| created	      | 	Date (with time)	     | 	Created	                                                            |

