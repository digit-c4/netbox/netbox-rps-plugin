## netbox_rps_plugin_httpheader

**HTTP Header table definition**

| 	FIELD	             | 	TYPE	                 | 	DESCRIPTION	                                                        |
|---------------------|------------------------|----------------------------------------------------------------------|
| 	apply_to	          | 	String (up to 30)	    | 	Apply to	                                                           |
| 	created	           | 	Date (with time)	     | 	created	                                                            |
| 	custom_field_data	 | 	A JSON object	        | 	custom field data	                                                  |
| 	get_absolute_url	  | 		                     | 	override	                                                           |
| 	id	                | 	Big (8 byte) integer	 | 	ID	                                                                 |
| 	last_updated	      | 	Date (with time)	     | 	last updated	                                                       |
| 	mapping_id         | 	Mapping	              | 	Refer [netbox_rps_plugin_mapping.id](netbox_rps_plugin_mapping.md)	 |
| 	name	              | 	String (up to 120)	   | 	Header name	                                                        |
| 	value	             | 	String (up to 256)	   | 	Header value	                                                       |

