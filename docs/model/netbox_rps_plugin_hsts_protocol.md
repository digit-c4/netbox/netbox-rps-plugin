## netbox_rps_hstsprotocol

**HSTS Protocol definition class**

| 	FIELD	             | 	TYPE	                           | 	DESCRIPTION	                                                        |
|---------------------|----------------------------------|----------------------------------------------------------------------|
| 	id	                | 	Big (8 byte) integer	           | 	ID	                                                                 |
| 	subdomains	           | 	Boolean (Either True or False)	               | 	includeSubDomains directive 	            |
| 	preload_flag	           | 	Boolean (Either True or False)	               | 	To prevent the first connection risk 	 |
| 	max_age	 | 	integer	     | 	HSTS max-age property, at least, 31536000 seconds (1 year). Options: 31536000 seconds (Default value), 63072000 seconds or 0   |
| 	last_updated	      | 	Date (with time)	               | 	last updated	                                                       |
| 	logout_url	        | 	String (up to 2000)	            | 	Logout URL	                                                         |
| 	mapping	           | 	Mapping	                        | 	Refer [netbox_rps_plugin_mapping.id](netbox_rps_plugin_mapping.md)	 |
| 	tags.count	        | 	Integer	                        | 	number of related extras.Tag objects	                               |