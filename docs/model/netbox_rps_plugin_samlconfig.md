## netbox_rps_plugin_samlconfig

**SAML config table definition**

| 	FIELD	             | 	TYPE	                           | 	DESCRIPTION	                                                        |
|---------------------|----------------------------------|----------------------------------------------------------------------|
| 	acs_url	           | 	String (up to 2000)	            | 	ACS URL	                                                            |
| 	created	           | 	Date (with time)	               | 	created	                                                            |
| 	custom_field_data	 | 	A JSON object	                  | 	custom field data	                                                  |
| 	force_nauth	       | 	Boolean (Either True or False)	 | 	force nauth	                                                        |
| 	id	                | 	Big (8 byte) integer	           | 	ID	                                                                 |
| 	last_updated	      | 	Date (with time)	               | 	last updated	                                                       |
| 	logout_url	        | 	String (up to 2000)	            | 	Logout URL	                                                         |
| 	mapping_id         | 	Mapping	                        | 	Refer [netbox_rps_plugin_mapping.id](netbox_rps_plugin_mapping.md)	 |
