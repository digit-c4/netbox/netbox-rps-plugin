$version: "2.0"

namespace eu.europa.ec.snet.rps

use aws.protocols#restJson1

@title("Netbox RPS Plugin")
@paginated(inputToken: "nextToken",
    outputToken: "nextToken", pageSize: "pageSize")
@restJson1
service rps {
    version: "0.15.0"
    resources: [Mapping, HttpHeader, SamlConfig, CacheConfig, HstsProtocol, ACLDeniedSource, MappingRedirect]
}

@timestampFormat("date-time")
timestamp DatesTime

@length(max: 10)
@pattern("^\\d{4}\\-(0[1-9]|1[012])\\-(0[1-9]|[12][0-9]|3[01])$")
string Dates

@pattern("^[0-9]+$")
string Ids