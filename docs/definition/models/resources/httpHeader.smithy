$version: "2.0"

namespace eu.europa.ec.snet.rps


@tags(["HttpHeader"])
@documentation("Resource - HttpHeader Table")
resource HttpHeader {
    identifiers: {id:String}
    properties: {mapping: MappingIds,
                 apply_to: String,
                 name: String,
                 value: String,
                 created: DatesTime,
                 custom_field_data: String,
                 last_updated: DatesTime,
                 tags: TagsList}
    create: CreateHttpheader
    read: GetHttpheader
    update: UpdateHttpHeader
    delete: DeleteHttpHeader
    list: ListHttpHeader
}


@tags(["HttpHeader"])
@documentation("Create new HttpHeader")
@http(method: "POST", uri: "/api/plugins/rps/httpheader")
operation CreateHttpheader {
    input := for HttpHeader {
        @required
        $mapping
        @required
        $apply_to
        @required
        $name

        $value
        $created
        $custom_field_data
        $last_updated
    }

    output := for HttpHeader {
        @required
        $id
        @required
        $mapping
        @required
        $apply_to
        @required
        $name

        $value
        $created
        $custom_field_data
        $last_updated
    }

    errors: [NotUnique]
}

@tags(["HttpHeader"])
@documentation("Show HttpHeader based on ID")
@readonly
@http(method: "GET", uri: "/api/plugins/rps/httpheader/{id}")
operation GetHttpheader {
    input := for HttpHeader {
        @required
        @httpLabel
        $id
    }

    output := for HttpHeader {
        @required
        $mapping
        @required
        $apply_to
        @required
        $name


        $value
        $created
        $custom_field_data
        $last_updated
        $tags
    }
}

@tags(["HttpHeader"])
@documentation("Update a HttpHeader")
@http(method: "PATCH", uri: "/api/plugins/rps/httpheader/{id}")
operation UpdateHttpHeader {
    input := for HttpHeader {
        @required
        @httpLabel
        $id
        $mapping
        $apply_to
        $name


        $value
        $created
        $custom_field_data
        $last_updated
    }

        errors: [NotUnique]
}

@tags(["HttpHeader"])
@documentation("Delete a HttpHeader")
@idempotent
@http(method: "DELETE", uri: "/api/plugins/rps/httpheader/{id}")
operation DeleteHttpHeader {
    input := for HttpHeader {
        @required
        @httpLabel
        $id
    }
}


@tags(["HttpHeader"])
@documentation("List HttpHeader")
@readonly
@http(method: "GET",uri: "/api/plugins/rps/httpheader/")
@paginated(inputToken: "nextToken", outputToken: "nextToken", pageSize: "maxResults", items: "Httpheaders")
operation ListHttpHeader {
    input := for HttpHeader {
        @httpQuery("maxResults")
        maxResults: Integer 
        @httpQuery("nextToken")
        nextToken: String 
    }

    output := for HttpHeader {
        nextToken: String
        @required
        Httpheaders: HttpheaderList
    }
}
@documentation("Unique constraint to prevent duplication of IDs")
@uniqueItems
list HttpheaderList
{
    member: HttpheaderSummary
}
@references(
[
    {resource: HttpHeader}
]
)
structure HttpheaderSummary for HttpHeader {
        @required
        $id
        @required
        $mapping
        @required
        $apply_to
        @required
        $name


        $value
        $created
        $custom_field_data
        $last_updated
        $tags
}
