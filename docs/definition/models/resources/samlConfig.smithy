$version: "2.0"

namespace eu.europa.ec.snet.rps

@tags(["SamlConfig"])
@documentation("Resource - SamlConfig Table")
resource SamlConfig {
    identifiers: {id:String}
    properties: {mapping: MappingIds,
                 acs_url: Url,
                 force_nauth: Boolean,
                 logout_url: Url,
                 created: DatesTime,
                 custom_field_data: String,
                 last_updated: DatesTime,
                 tags: TagsList}
    create: CreateSamlconfig
    read: GetSamlconfig
    update: UpdateSamlConfig
    delete: DeleteSamlConfig
    list: ListSamlConfig
}

@tags(["SamlConfig"])
@documentation("Create new SamlConfig")
@http(method: "POST", uri: "/api/plugins/rps/samlconfig/")
operation CreateSamlconfig {
    input := for SamlConfig {
        @required
        $mapping
        @required
        $acs_url
        @required
        $force_nauth
        @required
        $logout_url

        $created
        $custom_field_data
        $last_updated
    }

    output := for SamlConfig {
        @required
        $id
        @required
        $mapping
        @required
        $acs_url
        $force_nauth
        @required
        $logout_url
        @required

        $created
        $custom_field_data
        $last_updated

    }
     errors: [NotUnique]
}

@tags(["SamlConfig"])
@documentation("Show SamlConfig based on ID")
@readonly
@http(method: "GET", uri: "/api/plugins/rps/samlconfig/{id}")
operation GetSamlconfig {
    input := for SamlConfig {
       @required
        @httpLabel
        $id
    }

    output := for SamlConfig {
        @required
        $mapping
        @required
        $acs_url
        @required
        $force_nauth
        @required
        $logout_url
        @required

        $created
        $custom_field_data
        $last_updated
        $tags
    }
}


@tags(["SamlConfig"])
@documentation("Update a SamlConfig")
@http(method: "PATCH", uri: "/api/plugins/rps/samlconfig/{id}")
operation UpdateSamlConfig {
    input := for SamlConfig {
        @required
        @httpLabel
        $id
        $mapping
        $acs_url
        $force_nauth
        $logout_url

        $created
        $custom_field_data
        $last_updated
    }

     errors: [NotUnique]
}

@tags(["SamlConfig"])
@documentation("Delete a SamlConfig")
@idempotent
@http(method: "DELETE", uri: "/api/plugins/rps/samlconfig/{id}")
operation DeleteSamlConfig {
    input := for SamlConfig {
        @required
        @httpLabel
        $id
    }
}

@tags(["SamlConfig"])
@documentation("List SamlConfig")
@readonly
@http(method: "GET",uri: "/api/plugins/rps/samlconfig/")
@paginated(inputToken: "nextToken", outputToken: "nextToken", pageSize: "maxResults", items: "SamlConfigs")
operation ListSamlConfig {
    input := for SamlConfig {
        @httpQuery("maxResults")
        maxResults: Integer 
        @httpQuery("nextToken")
        nextToken: String 
    }

    output := for SamlConfig {
        nextToken: String
        @required
        SamlConfigs: SamlConfigList
    }

}
@documentation("Unique constraint to prevent duplication of IDs")
@uniqueItems
list SamlConfigList {

    member: SamlConfigSummary
}
@references(
[
    {resource: SamlConfig}
]
)
structure SamlConfigSummary for SamlConfig {
        @required
        $id
        @required
        $mapping
        @required
        $acs_url
        @required
        $force_nauth
        @required
        $logout_url

        $created
        $custom_field_data
        $last_updated
        $tags
}
