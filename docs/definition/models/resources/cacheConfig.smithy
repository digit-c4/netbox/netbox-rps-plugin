$version: "2.0"

namespace eu.europa.ec.snet.rps

@tags(["CacheConfig"])
@documentation("Resource - CacheConfig Table")
resource CacheConfig {
    identifiers: {id:String}
    properties: {idMapping: MappingId,
                 list_extensions: Extensions,
                 ttl: Integer,
                 max_size_limit: Integer,
                 created: DatesTime,
                 custom_field_data: String,
                 last_updated: DatesTime,
                 tags: TagsList
    }
    create: CreateCacheConfig
    read: GetCacheConfig
    update: UpdateCacheConfig
    delete: DeleteCacheConfig
    list: ListCacheConfig
}

@tags(["CacheConfig"])
@documentation("Create new CacheConfig")
@http(method: "POST", uri: "/api/plugins/rps/CacheConfig/")
operation CreateCacheConfig {
    input := for CacheConfig {
        @required
        $list_extensions
        @required
        $ttl
        @required
        $max_size_limit
             
    }

    output := for CacheConfig {
        @required
        $id
        @required
        $list_extensions
        $ttl
        @required
        $max_size_limit

        $created
        $custom_field_data
        $last_updated       
    }
     errors: [NotUnique]   
}

@tags(["CacheConfig"])
@documentation("Show CacheConfig based on ID")
@readonly
@http(method: "GET", uri: "/api/plugins/rps/CacheConfig/{id}")
operation GetCacheConfig {
    input := for CacheConfig {
       @required
        @httpLabel
        $id
    }

    output := for CacheConfig {
        @required
        $idMapping
        @required
        $list_extensions
        @required
        $ttl
        @required
        $max_size_limit

        $created
        $custom_field_data
        $last_updated
        $tags
    }
}


@tags(["CacheConfig"])
@documentation("Update a CacheConfig")
@http(method: "PATCH", uri: "/api/plugins/rps/CacheConfig/{id}")
operation UpdateCacheConfig {
    input := for CacheConfig {
        @required
        @httpLabel
        $id
        @required
        $list_extensions
        @required
        $ttl
        @required
        $max_size_limit

        $created
        $custom_field_data
        $last_updated      
    }

     errors: [NotUnique]   
}

@tags(["CacheConfig"])
@documentation("Delete a CacheConfig")
@idempotent
@http(method: "DELETE", uri: "/api/plugins/rps/CacheConfig/{id}")
operation DeleteCacheConfig {
    input := for CacheConfig {
        @required
        @httpLabel
        $id
    }
    output := for CacheConfig {
        @required
        $id
    }
}

@tags(["CacheConfig"])
@documentation("List CacheConfig")
@readonly
@http(method: "GET",uri: "/api/plugins/rps/cacheconfigs/")
@paginated(inputToken: "nextToken", outputToken: "nextToken", pageSize: "maxResults", items: "CacheConfigs")
operation ListCacheConfig {
    input := for Mapping {
        @httpQuery("maxResults")
        maxResults: Integer 
        @httpQuery("nextToken")
        nextToken: String 
    }

    output := for Mapping {
        nextToken: String
        @required
        CacheConfigs: CacheConfigList
    }
}
 


@sparse
list Extensions {
    member: File
}
enum File {
    JPG
    GIF
    CSS
    HTML
    JS
    XML
    OTHER
}



@documentation("Unique constraint to prevent duplication of IDs")
@uniqueItems
list CacheConfigList {

    member: CacheConfigSummary
}



@references(
[
    {resource: CacheConfig}
]
)
structure CacheConfigSummary for CacheConfig {
        @required
        $id
        @required
        $idMapping
        @required
        $list_extensions
        @required
        $ttl


        $created
        $custom_field_data
        $last_updated
        $tags
}

 
