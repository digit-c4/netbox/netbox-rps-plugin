$version: "2.0"

namespace eu.europa.ec.snet.rps

@tags(["HstsProtocol"])
@documentation("Resource - SamlProtocol Table")
resource HstsProtocol {
    identifiers: {id:String}
    properties: {mapping: MappingIds,
                 max_age: MaxAge,
                 subdomains: Boolean,
                 preload_flag: Boolean,
                 created: Timestamp,
                 custom_field_data: String,
                 last_updated: Timestamp,
                 tags: TagsList }
    create: CreateHstsProtocol
    read: GetHstsProtocol
    update: UpdateHstsProtocol
    delete: DeleteHstsProtocol
    list: ListHstsProtocol
}

@tags(["HstsProtocol"])
@documentation("Create new hstsconfig")
@http(method: "POST", uri: "/api/plugins/rps/hstsconfig/")
operation CreateHstsProtocol {
    input := for HstsProtocol {

        @required
        $max_age

        $subdomains
        $preload_flag
        $created
        $custom_field_data
        $tags    
    }

    output := for HstsProtocol {
        @required
        $id

    }
     errors: [NotUnique]
}

@tags(["HstsProtocol"])
@documentation("Show HstsProtocol based on ID")
@readonly
@http(method: "GET", uri: "/api/plugins/rps/hstsconfig/{id}")
operation GetHstsProtocol {
    input := for HstsProtocol {
       @required
        @httpLabel
        $id
    }

    output := for HstsProtocol {
        @required
        $max_age
        @required
        $mapping

        $subdomains
        $preload_flag
        $last_updated
        $created
        $custom_field_data
        $tags    
    }
}


@tags(["HstsProtocol"])
@documentation("Update a HstsProtocol")
@http(method: "PATCH", uri: "/api/plugins/rps/hstsconfig/{id}")
operation UpdateHstsProtocol {
    input := for HstsProtocol {
        @required
        @httpLabel
        $id

        $max_age
        $subdomains
        $preload_flag
        $tags

    }

     errors: [NotUnique]
}

@tags(["HstsProtocol"])
@documentation("Delete a HstsProtocol")
@idempotent
@http(method: "DELETE", uri: "/api/plugins/rps/hstsconfig/{id}")
operation DeleteHstsProtocol {
    input := for HstsProtocol {
        @required
        @httpLabel
        $id
    }
}

@tags(["HstsProtocol"])
@documentation("List HstsProtocol")
@readonly
@http(method: "GET",uri: "/api/plugins/rps/hstsconfig/")
@paginated(inputToken: "nextToken", outputToken: "nextToken", pageSize: "maxResults", items: "HstsProtocols")
operation ListHstsProtocol {
    input := for HstsProtocol {
        @httpQuery("maxResults")
        maxResults: Integer 
        @httpQuery("nextToken")
        nextToken: String 
        @httpQuery("mappingId")
        mappingId: String
    }

    output := for HstsProtocol {
        nextToken: String
        @required
        HstsProtocols: HstsProtocolList
    }

}
@documentation("Unique constraint to prevent duplication of IDs")
@uniqueItems
list HstsProtocolList {

    member: HstsProtocolSummary
}
@references(
[
    {resource: HstsProtocol}
]
)
structure HstsProtocolSummary for HstsProtocol {
        @required
        $id
        @required
        $mapping
        @required
        $max_age

        $subdomains
        $preload_flag
        $last_updated
        $created
        $custom_field_data
        $tags
}

intEnum MaxAge {
    HSTSREMOVE = 0
    HSTSONEYEAR = 31536000
    HSTSTWOYEARS = 63072000
}
