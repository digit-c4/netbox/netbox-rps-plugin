$version: "2.0"

namespace eu.europa.ec.snet.rps


@tags(["Mapping"])
@documentation("Resource - Mapping Table")
resource Mapping {
    identifiers: {id: MappingId}
    properties: {source: Url,
                 target: Url,
                 comment: String,
                 authentication: String,
                 client_max_body_size: Integer,
                 created: DatesTime,
                 custom_field_data: String,
                 extra_protocols: ExtraProtocolsList,
                 gzip_proxied: String,
                 keepalive_request: String,
                 keepalive_timeout: String,
                 last_updated: DatesTime,
                 proxy_cache: Boolean,
                 proxy_read_timeout: Integer,
                 sorry_page: String,
                 testingpage: Url,
                 webdav: Boolean,
                 proxy_buffer_size: Integer,
                 proxy_buffer: Integer,
                 proxy_busy_buffer: Integer,
                 http_headers: HttpheaderMappingList,
                 saml_config: SamlConfigMapping,
                 cache_config: CacheConfigMappingList,
                 hsts_protocol: HstsProtocolMapping,
                 url: Url,
                 tags: TagsList}
    create: CreateMapping
    read: GetMapping
    update: UpdateMapping
    delete: DeleteMapping
    list: ListMapping
}

@tags(["Mapping"])
@documentation("Show Mapping based on ID")
@readonly
@http(method: "GET", uri: "/api/plugins/rps/mapping/{id}")
operation GetMapping {
    input := for Mapping {
        @required
        @httpLabel
        $id
    }

    output := for Mapping {
        @required
        $url
        @required
        $target
        @required
        $source

        $comment
        @required
        $authentication
        @required
        $client_max_body_size

        $created
        $custom_field_data
        $extra_protocols

        @required
        $gzip_proxied
        @required
        $keepalive_request
        @required
        $keepalive_timeout
        $last_updated
        @required
        $proxy_cache
        @required
        $proxy_read_timeout
        @required
        $sorry_page

        $testingpage

        @required
        $webdav
        @required
        $proxy_buffer_size
        @required
        $proxy_buffer
        @required
        $proxy_busy_buffer
        @required
        $tags

        $http_headers

        $saml_config

        $cache_config

        $hsts_protocol

    }
}


@tags(["Mapping"])
@documentation("Create new Mapping")
@idempotent
@http(method: "POST", uri: "/api/plugins/rps/mapping/")
operation CreateMapping {
    input := for Mapping {

        @required
        $target
        @required
        $source

        $comment
        @required
        $authentication
        @required
        $client_max_body_size

        $created
        $custom_field_data
        $extra_protocols
        @required
        $gzip_proxied
        @required
        $keepalive_request
        @required
        $keepalive_timeout

        $last_updated

        @required
        $proxy_cache
        @required
        $proxy_read_timeout
        @required
        $sorry_page

        $testingpage

        @required
        $webdav

        @required
        $proxy_buffer_size
        @required
        $proxy_buffer
        @required
        $proxy_busy_buffer

    }

    output := for Mapping {
        @required
        $id
        @required
        $target
        @required
        $source

        $comment

        @required
        $authentication
        @required
        $client_max_body_size

        $created
        $custom_field_data
        $extra_protocols

        @required
        $gzip_proxied
        @required
        $keepalive_request
        @required
        $keepalive_timeout

        $last_updated

        @required
        $proxy_cache
        @required
        $proxy_read_timeout
        @required
        $sorry_page

        $testingpage

        @required
        $webdav

        @required
        $proxy_buffer_size
        @required
        $proxy_buffer
        @required
        $proxy_busy_buffer

    }
        errors: [NotUnique]
}

@tags(["Mapping"])
@documentation("Update a Mapping")
@http(method: "PATCH", uri: "/api/plugins/rps/mapping/{id}")
operation UpdateMapping {
    input := for Mapping {
        @required
        @httpLabel
        $id

        $target
        $source
        $comment
        $authentication
        $client_max_body_size
        $created
        $custom_field_data
        $extra_protocols
        $gzip_proxied
        $keepalive_request
        $keepalive_timeout
        $last_updated
        $proxy_cache
        $proxy_read_timeout
        $sorry_page
        $testingpage
        $webdav
        $proxy_buffer_size
        $proxy_buffer
        $proxy_busy_buffer
    }

        errors: [NotUnique]
}

@tags(["Mapping"])
@documentation("Delete a Mapping")
@idempotent
@http(method: "DELETE", uri: "/api/plugins/rps/mapping/{id}")
operation DeleteMapping {
    input := for Mapping {
        @required
        @httpLabel
        $id
    }
}


@tags(["Mapping"])
@documentation("List Mappings")
@readonly
@http(method: "GET",uri: "/api/plugins/rps/mappings/")
@paginated(inputToken: "nextToken", outputToken: "nextToken", pageSize: "maxResults", items: "Mappings")
operation ListMapping {
    input := for Mapping {
        @httpQuery("maxResults")
        maxResults: Integer
        @httpQuery("nextToken")
        nextToken: String
    }

    output := for Mapping {
        nextToken: String
        @required
        Mappings: MappingList
    }
}

@documentation("Unique constraint to prevent duplication of IDs")
@uniqueItems
list MappingList {

    member: MappingSummary
}
@references(
[
    {resource: Mapping}
]
)
structure MappingSummary for Mapping {
    @required
    $id
    @required
    $url
    @required
    $target
    @required
    $source

    $comment

    @required
    $authentication
    @required
    $client_max_body_size

    $created
    $custom_field_data
    $extra_protocols

    @required
    $gzip_proxied
    @required
    $keepalive_request
    @required
    $keepalive_timeout

    $last_updated
    @required
    $proxy_cache
    @required
    $proxy_read_timeout
    @required
    $sorry_page

    $testingpage
    @required
    $webdav
    @required
    $proxy_buffer_size
    @required
    $proxy_buffer
    @required
    $proxy_busy_buffer
    @required
    $tags

    $http_headers

    $saml_config

    $cache_config

    $hsts_protocol
}



@uniqueItems
list HttpheaderMappingList
{
    member: HttpheaderMapping
}
@references(
[
    {resource: HttpHeader}
]
)
structure HttpheaderMapping for HttpHeader {
        @required
        $id
        @required
        $apply_to
        @required
        $name

        $value

}


@references(
[
    {resource: SamlConfig}
]
)
structure SamlConfigMapping for SamlConfig {
        @required
        $id
        @required
        $acs_url
        @required
        $force_nauth
        @required
        $logout_url
}

@references(
[
    {resource: HstsProtocol}
]
)
structure HstsProtocolMapping for HstsProtocol {
        @required
        $id
        @required
        $max_age

        $subdomains
        $preload_flag
}



list ExtraProtocolsList
    {
        member: String
    }


@references(
[
    {resource: Mapping}
]
)
structure MappingIds for Mapping {
        @required
        $id

        $url
        display: String
}


list CacheConfigMappingList
{
    member: CacheConfigMapping
}

structure CacheConfigMapping for CacheConfig {
        @required
        $id
        @required
        $list_extensions
        @required
        $ttl
        @required
        $max_size_limit
}


@uniqueItems
list TagsList
{
    member: Tags
}
structure Tags {
        @required
        id: Id

        url: Url
        display: String
        name: String
        slug: String
        color: String

}

@pattern("^[0-9]+$")
string MappingId
string Url
integer Id
