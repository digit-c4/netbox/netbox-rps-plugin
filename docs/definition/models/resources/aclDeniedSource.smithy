$version: "2.0"

namespace eu.europa.ec.snet.rps

@tags(["ACLDeniedSource"])
@documentation("Resource - Acl denied source Table")
resource ACLDeniedSource {
    identifiers: {id: String}
    properties: {mapping_id: MappingIds,
                 url: Url,
                 acl_source: String,
                 created: Timestamp,
                 last_updated: Timestamp,
                 tags: TagsList}
    create: CreateACLDeniedSource
    read: GetACLDeniedSource
    update: UpdateACLDeniedSource
    delete: DeleteACLDeniedSource
    list: ListACLDeniedSource
}

@tags(["ACLDeniedSource"])
@documentation("Create new ACLDeniedSource")
@http(method: "POST", uri: "/api/plugins/rps/acldeniedsource/")
operation CreateACLDeniedSource {
    input := for ACLDeniedSource {
        $mapping_id
        $acl_source
        $created
        $last_updated
    }

    output := for ACLDeniedSource {
        @required
        $id
        $mapping_id
        $acl_source
        $created
        $last_updated

    }
    errors: [NotUnique]
}

@tags(["ACLDeniedSource"])
@documentation("Show ACLDeniedSource based on ID")
@readonly
@http(method: "GET", uri: "/api/plugins/rps/acldeniedsource/{id}")
operation GetACLDeniedSource {
    input := for ACLDeniedSource {
        @required
        @httpLabel
        $id
    }

    output := for ACLDeniedSource {
        $url
        $mapping_id
        $acl_source
        $created
        $last_updated
        $tags
    }
}


@tags(["ACLDeniedSource"])
@documentation("Update a ACLDeniedSource")
@http(method: "PATCH", uri: "/api/plugins/rps/acldeniedsource/{id}")
operation UpdateACLDeniedSource {
    input := for ACLDeniedSource {
        @required
        @httpLabel
        $id
        $mapping_id
        $acl_source
        $created
        $last_updated
    }

    errors: [NotUnique]
}

@tags(["ACLDeniedSource"])
@documentation("Delete a ACLDeniedSource")
@idempotent
@http(method: "DELETE", uri: "/api/plugins/rps/acldeniedsource/{id}")
operation DeleteACLDeniedSource {
    input := for ACLDeniedSource {
        @required
        @httpLabel
        $id
    }
}

@tags(["ACLDeniedSource"])
@documentation("List ACLDeniedSource")
@readonly
@http(method: "GET", uri: "/api/plugins/rps/acldeniedsource/")
@paginated(inputToken: "nextToken", outputToken: "nextToken", pageSize: "maxResults", items: "ACLDeniedSource")
operation ListACLDeniedSource {
    input := for ACLDeniedSource {
        @httpQuery("maxResults")
        maxResults: Integer
        @httpQuery("nextToken")
        nextToken: String
    }

    output := for ACLDeniedSource {
        nextToken: String
        @required
        ACLDeniedSource: ACLDeniedSourceList
    }

}

@documentation("Unique constraint to prevent duplication of IDs")
@uniqueItems
list ACLDeniedSourceList {

    member: ACLDeniedSourceSummary
}

@references(
[
    {resource: ACLDeniedSource}
]
)
structure ACLDeniedSourceSummary for ACLDeniedSource {
    $id
    $mapping_id
    $url
    $acl_source
    $created
    $last_updated
    $tags
}

@documentation("Unique constraint to prevent duplication of IDs")
@uniqueItems
list ACLDeniedSourceMappingList {

    member: ACLDeniedSourceMappingSummary
}

@references(
[
    {resource: ACLDeniedSource}
]
)

structure ACLDeniedSourceMappingSummary for ACLDeniedSource {
    $id
    $url
    $acl_source
}
