$version: "2.0"

namespace eu.europa.ec.snet.rps

//Get error when ID not unique
@documentation("Error used for failure to garantee uniqueness of IDs")
@error("client")
structure NotUnique {
    @required
    message: String = "Unique Constraint Failed"
}


//Get error when ID not unique
@documentation("Error used for failure to garantee deactivation date is after activation date")
@error("client")
structure DateValidation {
    @required
    message: String = "Deactivation date should be afer activation date"
}