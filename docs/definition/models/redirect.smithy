$version: "2.0"

namespace eu.europa.ec.snet.rps

@tags(["Redirect"])
@documentation("Resource - Mapping Redirect Table")
resource MappingRedirect {
    identifiers: {id:String}
    properties: {mappings: MappingIds, mapping: Ids, source: String, target: String, redirect_type: redirect_types, activation_date: Dates, deactivation_date: Dates}
    create: CreateMappingRedirect
    read: GetMappingRedirect
    update: UpdateMappingRedirect
    delete: DeleteMappingRedirect
    list: ListMappingRedirect
}

@tags(["Redirect"])
@documentation("Create new Mapping Redirect")
@http(method: "POST", uri: "/api/plugins/rps/redirect/")
operation CreateMappingRedirect {
    input := for MappingRedirect {

        @required
        $target
        $source
        $mapping
        @required
        $redirect_type

        $activation_date
        $deactivation_date

    }

    output := for MappingRedirect {
        @required
        $id     

    }
     errors: [NotUnique,DateValidation]   
}

@tags(["Redirect"])
@documentation("Show Mapping Redirects based on ID")
@readonly
@http(method: "GET", uri: "/api/plugins/rps/redirect/{id}")
operation GetMappingRedirect {
    input := for MappingRedirect {
       @required
        @httpLabel
        $id
    }

    output := for MappingRedirect {
        @required
        $id
        @required
        $target
        $source
        $mappings
        @required
        $redirect_type
        
        $activation_date
        $deactivation_date
    }
}


@tags(["Redirect"])
@documentation("Update a Mapping Redirect")
@http(method: "PATCH", uri: "/api/plugins/rps/redirect/{id}")
operation UpdateMappingRedirect {
    input := for MappingRedirect {
        @required
        @httpLabel
        $id

        $target
        $source
        $mapping
        $redirect_type
        $activation_date
        $deactivation_date

    }

     errors: [NotUnique,DateValidation]   
}

@tags(["Redirect"])
@documentation("Delete a Mapping Redirect")
@idempotent
@http(method: "DELETE", uri: "/api/plugins/rps/redirect/{id}")
operation DeleteMappingRedirect {
    input := for MappingRedirect {
        @required
        @httpLabel
        $id
    }
}

@tags(["Redirect"])
@documentation("List Mapping Redirects")
@readonly
@http(method: "GET",uri: "/api/plugins/rps/redirect/")
@paginated(inputToken: "next", outputToken: "next", pageSize: "limit", items: "results")
operation ListMappingRedirect {
    input := for MappingRedirect {
        @httpQuery("limit")
        limit: Integer 
        @httpQuery("offset")
        offset: String 
        @httpQuery("ordering")
        next: String
    }

    output := for MappingRedirect {
        count: Integer
        next: String
        previous: String
        @required
        results: MappingRedirectsList
    }

}
@documentation("Unique constraint to prevent duplication of IDs")
@uniqueItems
list MappingRedirectsList {

    member: MappingRedirectsSummary
}
@references(
[
    {resource: MappingRedirect}
]
)
structure MappingRedirectsSummary for MappingRedirect {
        @required
        $id
        @required
        $mappings
        @required
        $source
        @required
        $target
        @required
        $redirect_type
        
        $activation_date
        $deactivation_date

        
}

enum redirect_types {
    ONE_TO_ONE = "1-1"
    MANY_TO_MANY = "N-N"
    MANY_TO_ONE = "N-1"
}