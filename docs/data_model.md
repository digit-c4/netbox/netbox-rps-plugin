# RPS Plugin Data Model Documentation
*v1.2*

![schema_diagram.png](https://code.europa.eu/digit-c4/netbox/netbox-rps-plugin/-/jobs/artifacts/main/raw/.project/docs/model/schema_diagram.png?job=doc-job)

* [Mapping table definition](model/netbox_rps_plugin_mapping.md)

* [SAML config table definition](model/netbox_rps_plugin_samlconfig.md)

* [HTTP Header table definition](model/netbox_rps_plugin_samlconfig.md)

* [HSTS Protocol table definition](model/netbox_rps_plugin_hsts_protocol.md)

* [Cache Config table definition](model/netbox_rps_plugin_cacheconfig.md)

* [Redirect table definition](model/netbox_rps_plugin_redirect.md)
