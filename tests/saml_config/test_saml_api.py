"""Host API Test Case"""

from django.contrib.contenttypes.models import ContentType
from users.models import ObjectPermission # pylint: disable=import-error
from rest_framework import status
from utilities.testing import APIViewTestCases # pylint: disable=import-error
from netbox_rps_plugin.models import Mapping,SamlConfig
from tests.base import BaseAPITestCase

class MappingApiTestCase(
    BaseAPITestCase,
    APIViewTestCases.GetObjectViewTestCase,
    APIViewTestCases.ListObjectsViewTestCase,
    APIViewTestCases.CreateObjectViewTestCase,
    APIViewTestCases.UpdateObjectViewTestCase,
    APIViewTestCases.DeleteObjectViewTestCase,
):
    """Mapping API Test Case Class"""

    model = SamlConfig
    brief_fields = [
        "acs_url",
        "force_nauth",
        "id",
        "logout_url",
        "url",
    ]

    @classmethod
    def setUpTestData(cls) -> None: # pylint: disable=invalid-name
        """Initial Data setup to perform the 'parent' ViewTestCases"""
        acs_url_tmp="https://truc11.com/api"
        logout_url_tmp="http://10.10.10.21:1800/api"

        mapp00= Mapping.objects.create(
                source="https://truc00.com/api", target="http://10.10.10.10:1800/api",)
        mapp01= Mapping.objects.create(
                source="https://truc01.com/api", target="http://10.10.10.11:1800/api",)
        mapp02= Mapping.objects.create(
                source="https://truc02.com/api", target="http://10.10.10.12:1800/api",)
        mapp03= Mapping.objects.create(
                source="https://truc03.com/api", target="http://10.10.10.13:1800/api",)
        mapp04= Mapping.objects.create(
                source="https://truc04.com/api", target="http://10.10.10.14:1800/api",)
        mapp05= Mapping.objects.create(
                source="https://truc05.com/api", target="http://10.10.10.15:1800/api",)

        SamlConfig.objects.create(
            acs_url=acs_url_tmp,
            logout_url=logout_url_tmp,
            force_nauth=False,
            mapping= mapp00,
        )
        SamlConfig.objects.create(
            acs_url=acs_url_tmp,
            logout_url=logout_url_tmp,
            force_nauth=False,
            mapping= mapp01,
        )
        SamlConfig.objects.create(
            acs_url=acs_url_tmp,
            logout_url=logout_url_tmp,
            force_nauth=False,
            mapping= mapp02,
        )

        cls.create_data = [
            {
                "acs_url": acs_url_tmp,
                "logout_url": logout_url_tmp,
                "force_nauth": False,
                "mapping": mapp03.pk,
            },
            {
                "acs_url": acs_url_tmp,
                "logout_url": logout_url_tmp,
                "force_nauth": False,
                "mapping": mapp04.pk,
            },
            {
                "acs_url": acs_url_tmp,
                "logout_url": logout_url_tmp,
                "force_nauth": False,
                "mapping": mapp05.pk,
            },
        ]

    def test_that_saml_is_unique(self) -> None:
        """Test that SAML's PK is unique"""

        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        # pylint: disable=E1101
        obj_perm.users.add(self.user)
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))

        mapp06= Mapping.objects.create(
                source="https://truc20.com/api", target="http://10.10.10.30:1800/api",)

        response = self.client.post(
            self._get_list_url(),
            {
                "acs_url": "https://truc21.com/api",
                "logout_url": "http://10.10.10.31:1888/api",
                "force_nauth": "False",
                "mapping": mapp06.pk,
            },
            format="json",
            **self.header,
        )
        self.assertHttpStatus(response, status.HTTP_201_CREATED)

        response = self.client.post(
            self._get_list_url(),
            {
                "acs_url": "https://truc22.com/api",
                "logout_url": "http://10.10.10.32:1888/api",
                "force_nauth": "False",
                "mapping": mapp06.pk,
            },
            format="json",
            **self.header,
        )
        self.assertHttpStatus(response, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.content, b'{"mapping":["Saml config with this Mapping already exists."]}'
        )

    def test_saml_urls(self) -> None:
        """Test URL values within SAML config"""

        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        # pylint: disable=E1101
        obj_perm.users.add(self.user)
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))

        mapp07= Mapping.objects.create(
                source="https://truc25.com/api", target="http://10.10.10.35:1800/api",)
        mapp08= Mapping.objects.create(
                source="https://truc26.com/api", target="http://10.10.10.36:1800/api",)
        mapp09= Mapping.objects.create(
                source="https://truc27.com/api", target="http://10.10.10.37:1800/api",)

        response = self.client.post(
            self._get_list_url(),
            {
                "acs_url": "truc22.com/api",
                "logout_url": "http://10.10.10.32:1888/api",
                "force_nauth": "False",
                "mapping": mapp07.pk,
            },
            format="json",
            **self.header,
        )
        self.assertHttpStatus(response, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.content, b'{"acs_url":["It must be a url"]}'
        )

        response = self.client.post(
            self._get_list_url(),
            {
                "acs_url": "https://truc22.com/api",
                "logout_url": "ttp://10.10.10.32/api",
                "force_nauth": "False",
                "mapping": mapp08.pk,
            },
            format="json",
            **self.header,
        )
        self.assertHttpStatus(response, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.content, b'{"logout_url":["It must be a url"]}'
        )

        response = self.client.post(
            self._get_list_url(),
            {
                "acs_url": None,
                "logout_url": "http://10.10.10.32:1888/api",
                "force_nauth": "0",
                "mapping": mapp09.pk,
            },
            format="json",
            **self.header,
        )
        self.assertHttpStatus(response, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.content, b'{"acs_url":["This field may not be null."]}'
        )

    def test_saml_boolean(self) -> None:
        """Test boolean values within SAML config"""

        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        # pylint: disable=E1101
        obj_perm.users.add(self.user)
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))

        mapp06= Mapping.objects.create(
                source="https://truc24.com/api", target="http://10.10.10.34:1800/api",)
        mapp07= Mapping.objects.create(
                source="https://truc25.com/api", target="http://10.10.10.35:1800/api",)
        mapp08= Mapping.objects.create(
                source="https://truc26.com/api", target="http://10.10.10.36:1800/api",)
        mapp09= Mapping.objects.create(
                source="https://truc27.com/api", target="http://10.10.10.37:1800/api",)

        response = self.client.post(
            self._get_list_url(),
            {
                "acs_url": "https://truc21.com/api",
                "logout_url": "http://10.10.10.31:1888/api",
                "force_nauth": "1",
                "mapping": mapp06.pk,
            },
            format="json",
            **self.header,
        )
        self.assertHttpStatus(response, status.HTTP_201_CREATED)

        response = self.client.post(
            self._get_list_url(),
            {
                "acs_url": "https://truc22.com/api",
                "logout_url": "http://10.10.10.32:1888/api",
                "force_nauth": "Fals",
                "mapping": mapp07.pk,
            },
            format="json",
            **self.header,
        )
        self.assertHttpStatus(response, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.content, b'{"force_nauth":["Must be a valid boolean."]}'
        )
        response = self.client.post(
            self._get_list_url(),
            {
                "acs_url": "https://truc22.com/api",
                "logout_url": "http://10.10.10.32:1888/api",
                "force_nauth": "7",
                "mapping": mapp08.pk,
            },
            format="json",
            **self.header,
        )
        self.assertHttpStatus(response, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.content, b'{"force_nauth":["Must be a valid boolean."]}'
        )

        response = self.client.post(
            self._get_list_url(),
            {
                "acs_url": "https://truc21.com/api",
                "logout_url": "http://10.10.10.31:1888/api",
                "force_nauth": None,
                "mapping": mapp09.pk,
            },
            format="json",
            **self.header,
        )
        self.assertHttpStatus(response, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.content, b'{"force_nauth":["This field may not be null."]}'
        )

    def test_saml_required_fields(self) -> None:
        """Test for required values within SAML config"""

        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        # pylint: disable=E1101
        obj_perm.users.add(self.user)
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))

        mapp06= Mapping.objects.create(
                source="https://truc24.com/api", target="http://10.10.10.34:1800/api",)
        mapp07= Mapping.objects.create(
                source="https://truc25.com/api", target="http://10.10.10.35:1800/api",)

        response = self.client.post(
            self._get_list_url(),
            {
                #"acs_url": "https://truc21.com/api",
                "logout_url": "http://10.10.10.31:1888/api",
                "force_nauth": "1",
                "mapping": mapp06.pk,
            },
            format="json",
            **self.header,
        )
        self.assertHttpStatus(response, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.content, b'{"acs_url":["This field is required."]}'
        )
        response = self.client.post(
            self._get_list_url(),
            {
                "acs_url": "https://truc21.com/api",
                #"logout_url": "http://10.10.10.31:1888/api",
                "force_nauth": "1",
                "mapping": mapp07.pk,
            },
            format="json",
            **self.header,
        )
        self.assertHttpStatus(response, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.content, b'{"logout_url":["This field is required."]}'
        )
        response = self.client.post(
            self._get_list_url(),
            {
                "acs_url": "https://truc21.com/api",
                "logout_url": "http://10.10.10.31:1888/api",
                "force_nauth": 0,
                #"mapping": mapp08.pk,
            },
            format="json",
            **self.header,
        )
        self.assertHttpStatus(response, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.content, b'{"mapping":["This field is required."]}'
        )
