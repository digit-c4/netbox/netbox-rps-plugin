"""RPS Views Test Case"""

from utilities.testing import ViewTestCases # pylint: disable=import-error
from tests.base import BaseModelViewTestCase
from netbox_rps_plugin.models import Mapping,SamlConfig


class CertificateViewsTestCase(
    BaseModelViewTestCase,
    ViewTestCases.EditObjectViewTestCase,
    ViewTestCases.DeleteObjectViewTestCase,
):
    """RPS Views Test Case Class"""

    model = SamlConfig

    @classmethod
    def setUpTestData(cls): # pylint: disable=invalid-name
        """Initial Data setup to perform the 'parent' ViewTestCases"""
        acs_url_tmp="https://truc11.com/api"
        logout_url_tmp="http://10.10.10.21:1808/api"

        mapp00= Mapping.objects.create(
                source="https://truc00.com/api", target="http://10.10.10.10:1808/api",)
        mapp01= Mapping.objects.create(
                source="https://truc01.com/api", target="http://10.10.10.11:1808/api",)
        mapp02= Mapping.objects.create(
                source="https://truc02.com/api", target="http://10.10.10.12:1808/api",)
        mapp03= Mapping.objects.create(
                source="https://truc03.com/api", target="http://10.10.10.13:1808/api",)
        mapp04= Mapping.objects.create(
                source="https://truc04.com/api", target="http://10.10.10.14:1808/api",)
        mapp05= Mapping.objects.create(
                source="https://truc05.com/api", target="http://10.10.10.15:1808/api",)

        saml00= SamlConfig.objects.create(
            acs_url=acs_url_tmp,
            logout_url=logout_url_tmp,
            force_nauth=False,
            mapping= mapp00,
        )
        saml01= SamlConfig.objects.create(
            acs_url=acs_url_tmp,
            logout_url=logout_url_tmp,
            force_nauth=False,
            mapping= mapp01,
        )
        cls.form_data = {
            "acs_url": acs_url_tmp,
            "logout_url": logout_url_tmp,
            "force_nauth": False,
            "mapping": mapp02.pk,
        }
        cls.csv_data = (
            "acs_url,logout_url,force_nauth,mapping",
            f"{acs_url_tmp},{logout_url_tmp},False,{mapp03.pk}",
            f"{acs_url_tmp},{logout_url_tmp},False,{mapp04.pk}",
            f"{acs_url_tmp},{logout_url_tmp},False,{mapp05.pk}",
        )
        cls.csv_update_data = (
            "id,acs_url",
            f"{saml00.pk},https://truc03,",
            f"{saml01.pk},10.10.10.33:1888",
        )
        cls.csv_delete_data = (
            "id,logout_url",
            f"{saml00.pk},https://truc03,",
            f"{saml01.pk},10.10.10.33:1888",
        )
