"""Host API Test Case"""

import json
from django.contrib.contenttypes.models import ContentType  # pylint: disable=import-error
from users.models import ObjectPermission  # pylint: disable=import-error
from rest_framework import status  # pylint: disable=import-error
from utilities.testing import APIViewTestCases  # pylint: disable=import-error
from netbox_rps_plugin.models import Mapping, HttpHeader
from tests.base import BaseAPITestCase


class HttpHeaderApiTestCase(
    BaseAPITestCase,
    APIViewTestCases.GetObjectViewTestCase,
    APIViewTestCases.ListObjectsViewTestCase,
    APIViewTestCases.CreateObjectViewTestCase,
    APIViewTestCases.UpdateObjectViewTestCase,
    APIViewTestCases.DeleteObjectViewTestCase,
):
    """HttpHeader API Test Case Class"""

    model = HttpHeader
    brief_fields = [
        "apply_to",
        "id",
        "name",
        "url",
        "value"
    ]

    @classmethod
    def setUpTestData(cls) -> None: # pylint: disable=invalid-name
        """Initial Data setup to perform the 'parent' ViewTestCases"""
        mapping = Mapping.objects.create(
            source="https://truc00.com/api", target="http://10.10.10.11:1800/api"
        )

        HttpHeader.objects.create(
            name="Header00",
            apply_to="request",
            mapping=mapping,
            value="Header 00"
        )
        HttpHeader.objects.create(
            name="Header01",
            apply_to="request",
            mapping=mapping,
            value="Header 01"
        )
        HttpHeader.objects.create(
            name="Header02",
            apply_to="request",
            mapping=mapping,
            value="Header 02"
        )

        cls.create_data = [
            {
                "name": "Header03",
                "apply_to": "request",
                "mapping": mapping.pk,
                "value": "Header 03",
            },
            {
                "name": "Header04",
                "apply_to": "request",
                "mapping": mapping.pk,
                "value": "Header 04",
            },
            {
                "name": "Header05",
                "apply_to": "request",
                "mapping": mapping.pk,
                "value": "Header 05",
            },
        ]

    def test_that_http_header_is_unique(self) -> None:
        """Test that http_header is unique"""

        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        # pylint: disable=E1101
        obj_perm.users.add(self.user)
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))

        mapping = Mapping.objects.create(
            source="https://truc10.com/api",
            target="http://10.10.10.10:1800/api"
        )

        response = self.client.post(
            self._get_list_url(),
            {
                "name": "Header10",
                "apply_to": "request",
                "mapping": mapping.pk,
                "value": "Header 10",
                "authentication": "none",
                "testingpage": None,
            },
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_201_CREATED)

        response = self.client.post(
            self._get_list_url(),
            {
                "name": "Header10",
                "apply_to": "request",
                "mapping": mapping.pk,
                "value": "Header 10",
                "authentication": "none",
                "testingpage": None,
            },
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_400_BAD_REQUEST)

        self.assertEqual(
            response.content,
            b'{"non_field_errors":["The fields mapping, name, apply_to must make a unique set."]}'
        )

        response = self.client.post(
            self._get_list_url(),
            {
                "name": "Header20",
                "apply_to": "request",
                "mapping": mapping.pk,
                "value": "Header 20",
                "authentication": "none",
                "testingpage": None,
            },
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_201_CREATED)

        content = json.loads(response.content)

        self.assertEqual(content["name"], "Header20")
        self.assertEqual(content["apply_to"], "request")
        self.assertEqual(content["mapping"]["id"], mapping.pk)
        self.assertEqual(content["mapping"]["display"], mapping.source)
        self.assertEqual(content["value"], "Header 20")

        response = self.client.get(
            self._get_list_url() + f"{content['id']}/",
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_200_OK)

        content = json.loads(response.content)

        self.assertEqual(content["name"], "Header20")
        self.assertEqual(content["apply_to"], "request")
        self.assertEqual(content["mapping"]["id"], mapping.pk)
        self.assertEqual(content["mapping"]["display"], mapping.source)
        self.assertEqual(content["value"], "Header 20")

    def test_that_http_header_update_is_unique(self) -> None:
        """ Test that http_header update is unique  """

        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        # pylint: disable=E1101
        obj_perm.users.add(self.user)
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))

        mapping = Mapping.objects.create(

            source="https://truc11.com/api",
            target="http://10.10.10.11:1800/api"
        )

        response = self.client.post(
            self._get_list_url(),
            {
                "name": "Header11",
                "apply_to": "request",
                "mapping": mapping.pk,
                "value": "Header 11",
                "authentication": "none",
                "testingpage": None,
            },
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_201_CREATED)

        content = json.loads(response.content)

        response = self.client.patch(
            self._get_list_url() + f"{content['id']}/",
            {
                "name": "NewHeader11",
                "apply_to": "request",
                "mapping": mapping.pk,
                "value": "New Header 11",
                "authentication": "none",
                "testingpage": None,
            },
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_200_OK)

        content = json.loads(response.content)

        response = self.client.get(
            self._get_list_url() + f"{content['id']}/",
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_200_OK)

        content = json.loads(response.content)

        self.assertEqual(content["name"], "NewHeader11")
        self.assertEqual(content["apply_to"], "request")
        self.assertEqual(content["mapping"]["id"], mapping.pk)
        self.assertEqual(content["mapping"]["display"], mapping.source)
        self.assertEqual(content["value"], "New Header 11")
