"""Host VIEW Test Case"""

from utilities.testing import ViewTestCases # pylint: disable=import-error
from netbox_rps_plugin.models import Mapping, HttpHeader
from tests.base import BaseModelViewTestCase


class HttpHeaderViewTestCase(
    BaseModelViewTestCase,
    ViewTestCases.GetObjectViewTestCase,
    ViewTestCases.ListObjectsViewTestCase,
    ViewTestCases.CreateObjectViewTestCase,
    ViewTestCases.DeleteObjectViewTestCase,
):
    """HttpHeader VIEW Test Case Class"""

    model = HttpHeader

    @classmethod
    def setUpTestData(cls) -> None: # pylint: disable=invalid-name
        """Initial Data setup to perform the 'parent' ViewTestCases"""
        mapp00 =  Mapping.objects.create(
            source="https://truc00.com/api", target="http://10.10.10.11:1800/api"
        )
        mapp01 =  Mapping.objects.create(
            source="https://truc01.com/api", target="http://10.10.10.12:1800/api"
        )

        http00= HttpHeader.objects.create(
            name="Header00",
            apply_to="request",
            mapping= mapp00,
            value="Header 00"
        )
        http01= HttpHeader.objects.create(
            name="Header01",
            apply_to="request",
            mapping= mapp00,
            value="Header 01"
        )
        http02= HttpHeader.objects.create(
            name="Header02",
            apply_to="request",
            mapping= mapp00,
            value="Header 02"
        )
        cls.form_data = {
            "name": "Header03",
            "apply_to": "request",
            "mapping" : mapp00.pk,
            "value": "Header 03",
        }
        cls.csv_data = (
            "name,apply_to,mapping,value",
            f"Header04,request,{mapp00.pk},Header 04",
            f"Header05,request,{mapp00.pk},Header 05",
            f"Header06,request,{mapp00.pk},Header 06",
        )
        cls.csv_update_data = (
            "id,mapping,value",
            f"{http00.pk},{mapp01},Header 10",
            f"{http01.pk},{mapp01},Header 11",
            f"{http02.pk},{mapp01},Header 12",
        )

        cls.bulk_edit_data = {"apply_to": "request", "mapping": mapp01.pk}
