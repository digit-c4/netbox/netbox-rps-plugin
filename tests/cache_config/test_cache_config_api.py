"""Host Cache Config API Test Case"""

import json
from django.contrib.contenttypes.models import ContentType
from users.models import ObjectPermission # pylint: disable=import-error
from rest_framework import status
from utilities.testing import APIViewTestCases # pylint: disable=import-error
from netbox_rps_plugin.models import Mapping, CacheConfig
from tests.base import BaseAPITestCase


class CacheConfigApiTestCase(
    BaseAPITestCase,
    APIViewTestCases.GetObjectViewTestCase,
    APIViewTestCases.ListObjectsViewTestCase,
    APIViewTestCases.CreateObjectViewTestCase,
    # APIViewTestCases.UpdateObjectViewTestCase,
    # APIViewTestCases.DeleteObjectViewTestCase,
):
    """Cache Configuration API Test Case Class"""

    model = CacheConfig
    brief_fields = sorted([
        "id",
        "mapping",
        "ttl",
        "max_size_limit",
        "list_extensions",
        "created",
        "last_updated",
        "url",
    ])

    @classmethod
    def setUpTestData(cls) -> None: # pylint: disable=invalid-name
        """Initial Data setup to perform the 'parent' ViewTestCases"""
        cls.mapping = Mapping.objects.create(
            source="https://truc00.com/api",
            target="http://10.10.10.11:1800/api"
        )

        # Info : Required to fulfill
        # 'CacheConfigApiTestCase.test_list_objects' tests.
        CacheConfig.objects.create(
            mapping=cls.mapping,
            list_extensions=['JPG'],
        )
        CacheConfig.objects.create(
            mapping=cls.mapping,
            list_extensions=['PNG'],
        )
        CacheConfig.objects.create(
            mapping=cls.mapping,
            list_extensions=['PDF'],
        )

        # Info : Required to fulfill
        # 'CacheConfigApiTestCase.test_create_object_without_permission' tests.
        cls.create_data = [
            {
                "mapping": cls.mapping.pk,
                "list_extensions": ['JPG'],
            },
        ]

    def tearDown(self) -> None:# pylint: disable=invalid-name
        """Method called immediately after the test method has been called and the result recorded."""
        Mapping.objects.all().delete()
        CacheConfig.objects.all().delete()
        super().tearDown()

    def test_that_adding_cache_config(self):
        """Test that adding cache config object from API works"""

        # Info : Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "view"]
        )
        obj_perm.save()
        # pylint: disable=E1101
        obj_perm.users.add(self.user)
        obj_perm.object_types.add(
            ContentType.objects.get_for_model(self.model)
        )

        mapping = self.mapping

        data = {
            "mapping": mapping.pk,
            "ttl": 42,
            "max_size_limit": 100,
            "list_extensions": ['JPG'],
        }
        response = self.client.post(self._get_list_url(), data,
                                    format="json", **self.header)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue("id" in response.data)
        self.assertEqual(response.data["ttl"], 42)

    def test_that_listing_cache_config(self):
        """Test that listing cache config object from API works"""

        # Info : Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "view"]
        )
        obj_perm.save()
        # pylint: disable=E1101
        obj_perm.users.add(self.user)
        obj_perm.object_types.add(
            ContentType.objects.get_for_model(self.model)
        )

        mapping = self.mapping

        data = {
            "mapping": mapping.pk,
            "ttl": 42,
            "max_size_limit": 100,
            "list_extensions": ['JPG'],
        }
        response = self.client.post(self._get_list_url(), data,
                                    format="json", **self.header)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        values = self.client.get(self._get_list_url(), **self.header)

        self.assertIsNotNone(json.loads(values.content)["results"])

    def test_that_editing_cache_config(self):
        """Test that editing cache config object from API works"""

        # Info : Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"]
        )
        obj_perm.save()
        # pylint: disable=E1101
        obj_perm.users.add(self.user)
        obj_perm.object_types.add(
            ContentType.objects.get_for_model(self.model)
        )

        mapping = self.mapping

        data = {
            "mapping": mapping.pk,
            "ttl": 42,
            "max_size_limit": 100,
            "list_extensions": ['JPG'],
        }
        response = self.client.post(self._get_list_url(), data,
                                    format="json", **self.header)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue("id" in response.data)

        cache_config = json.loads(response.content)

        data = {
            "mapping": mapping.pk,
            "ttl": 88,
            "max_size_limit": 100,
            "list_extensions": ['JPG'],
        }
        response = self.client.patch(
            self._get_list_url() +
            f"{cache_config['id']}/", data,
            format="json", **self.header
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["ttl"], 88)

    def test_that_deleting_cache_config(self):
        """Test that deleting Cache Config works"""

        # Info : Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "delete", "view"]
        )
        obj_perm.save()
        # pylint: disable=E1101
        obj_perm.users.add(self.user)
        obj_perm.object_types.add(
            ContentType.objects.get_for_model(self.model)
        )

        mapping = self.mapping

        data = {
            "mapping": mapping.pk,
            "ttl": 42,
            "max_size_limit": 100,
            "list_extensions": ['JPG'],
        }
        response = self.client.post(self._get_list_url(), data,
                                    format="json", **self.header)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue("id" in response.data)

        cache_config = json.loads(response.content)

        response = self.client.delete(
            self._get_list_url() +
            f"{cache_config['id']}/", data,
            format="json", **self.header
        )

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_that_template_exist(self):
        """Test that HTML template exists when we click on cache config ID"""

        # Info : Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "view"]
        )
        obj_perm.save()
        # pylint: disable=E1101
        obj_perm.users.add(self.user)
        content_type = ContentType.objects.get_for_model(CacheConfig)
        obj_perm.object_types.add(content_type)

        mapping = self.mapping

        data = {
            "mapping": mapping.pk,
            "ttl": 42,
            "max_size_limit": 100,
            "list_extensions": ['JPG'],
        }
        response = self.client.post(self._get_list_url(), data, format="json", **self.header)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        cache_config_id = response.data['id']
        cache_config_instance = CacheConfig.objects.get(pk=cache_config_id)

        response = self.client.get(
            self._get_detail_url(cache_config_instance), **self.header
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Verify that the response contains the expected data
        expected_data = {
            "id": cache_config_id,
            "url": f"http://testserver/api/plugins/rps/cache_config/{cache_config_id}/",
        }

        self.assertDictContainsSubset(expected_data, response.json())
