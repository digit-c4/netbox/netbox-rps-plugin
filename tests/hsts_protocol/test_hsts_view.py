"""HSTS  Views Test Case"""
from utilities.testing import ViewTestCases # pylint: disable=import-error
from netbox_rps_plugin.tests.base import BaseModelViewTestCase
from netbox_rps_plugin.models import Mapping, HstsProtocol

class HstsProtocolTestCase(
    BaseModelViewTestCase,
    ViewTestCases.CreateObjectViewTestCase,
    ViewTestCases.EditObjectViewTestCase,
    ViewTestCases.DeleteObjectViewTestCase,
):
    """Hsts Views Test Case Class"""

    model = HstsProtocol

    @classmethod
    def setUpTestData(cls):  # pylint: disable=invalid-name
        """Initial Data setup to perform the 'parent' ViewTestCases"""
        default_max_age = 31536000
        logout_url = "https://logout.com"
        mapping1 = Mapping.objects.create(
            source="https://truc00.com/api", target="http://10.10.10.11:1800/api"
        )
        mapping2 = Mapping.objects.create(
            source="https://truc01.com/api", target="http://10.10.10.12:1800/api"
        )
        mapping3 = Mapping.objects.create(
            source="https://truc02.com/api", target="http://10.10.10.13:1800/api"
        )

        hsts1 = HstsProtocol.objects.create(
            mapping=mapping1,
            subdomains=False,
            preload_flag=False,
            max_age=default_max_age,
            logout_url=logout_url,
        )

        hsts2 = HstsProtocol.objects.create(
            mapping=mapping2,
            subdomains=True,
            preload_flag=False,
            max_age=default_max_age,
            logout_url=logout_url,
        )

        cls.form_data = {
            "mapping": mapping3.pk,
            "subdomains": True,
            "preload_flag": False,
            "max_age": default_max_age,
            "logout_url": logout_url,
        }

        cls.csv_update_data = (
            "id,subdomains",
            f"{hsts1.pk},30,{True}",
            f"{hsts2.pk},1,{True}",
        )
