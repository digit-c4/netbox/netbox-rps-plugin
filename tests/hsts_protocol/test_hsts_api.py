"""HSTS Protocol Test case"""
# pylint: disable=duplicate-code

from django.contrib.contenttypes.models import ContentType  # pylint: disable=import-error
from users.models import ObjectPermission  # pylint: disable=import-error
from rest_framework import status  # pylint: disable=import-error
from netbox_rps_plugin.models import Mapping, HstsProtocol
from netbox_rps_plugin.tests.base import BaseAPITestCase


class HstsProtocolApiTestCase(
    BaseAPITestCase,
):
    """HSTS Protocol Test Case Class"""

    model = HstsProtocol
    brief_field = [
        "id",
        "max_age",
        "subdomains",
        "preload_flag",
    ]

    @classmethod
    def setUpTestData(cls) -> None:  # pylint: disable=invalid-name
        """Setup data to perform tests"""
        logout_url = "https://logout.com"
        mapp00= Mapping.objects.create(
                source="https://truc10.com/api", target="http://10.10.10.10:1800/api",)
        mapp01= Mapping.objects.create(
                source="https://truc11.com/api", target="http://10.10.10.11:1800/api",)
        mapp02= Mapping.objects.create(
                source="https://truc12.com/api", target="http://10.10.10.12:1800/api",)
        mapp03= Mapping.objects.create(
                source="https://truc13.com/api", target="http://10.10.10.13:1800/api",)
        mapp04= Mapping.objects.create(
                source="https://truc14.com/api", target="http://10.10.10.14:1800/api",)
        mapp05= Mapping.objects.create(
                source="https://truc15.com/api", target="http://10.10.10.15:1800/api",)

        HstsProtocol.objects.create(
            max_age=0, subdomains=True, preload_flag=True, logout_url=logout_url, mapping=mapp00
        )
        HstsProtocol.objects.create(
            max_age=63072000, subdomains=True, preload_flag=True, logout_url=logout_url, mapping=mapp01
        )
        HstsProtocol.objects.create(
            max_age=63072000, subdomains=True, preload_flag=False, logout_url=logout_url, mapping=mapp02
        )

        cls.create_data = [
            {
                "max_age": 63072000,
                "subdomains": False,
                "preload_flag": True,
                "mapping": mapp03.pk,
                "logout_url": logout_url,
            },
            {
                "max_age": 63072000,
                "subdomains": True,
                "preload_flag": False,
                "mapping": mapp04.pk,
                "logout_url": logout_url,
            },
            {
                "max_age": 31536000,
                "subdomains": True,
                "preload_flag": True,
                "mapping": mapp05.pk,
                "logout_url": logout_url,
            },
        ]

    def test_that_hsts_is_unique(self) -> None:
        """Test that HSTS's PK is unique"""

        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        # pylint: disable=E1101
        obj_perm.users.add(self.user)
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))

        mapp06= Mapping.objects.create(
                source="https://truc20.com/api", target="http://10.10.10.30:1800/api",)

        response = self.client.post(
            self._get_list_url(),
            {
                "max_age": 0,
                "subdomains": True,
                "preload_flag": True,
                "mapping": mapp06.pk,
                "logout_url": "https://logout.com"
            },
            format="json",
            **self.header,
        )
        self.assertHttpStatus(response, status.HTTP_201_CREATED)

        response = self.client.post(
            self._get_list_url(),
            {
                "max_age": 31536000,
                "subdomains": True,
                "preload_flag": True,
                "mapping": mapp06.pk,
                "logout_url": "https://logout.com"
            },
            format="json",
            **self.header,
        )
        self.assertHttpStatus(response, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.content, b'{"mapping":["Hsts protocol with this Mapping already exists."]}'
        )
