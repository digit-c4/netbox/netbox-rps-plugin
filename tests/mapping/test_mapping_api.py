"""Host API Test Case"""

import json
from django.contrib.contenttypes.models import ContentType
from users.models import ObjectPermission  # pylint: disable=import-error
from rest_framework import status
from utilities.testing import APIViewTestCases  # pylint: disable=import-error
from netbox_rps_plugin.models import Mapping
from tests.base import BaseAPITestCase


class MappingApiTestCase(
    BaseAPITestCase,
    APIViewTestCases.GetObjectViewTestCase,
    APIViewTestCases.ListObjectsViewTestCase,
    APIViewTestCases.CreateObjectViewTestCase,
    APIViewTestCases.UpdateObjectViewTestCase,
    APIViewTestCases.DeleteObjectViewTestCase,
):
    """Mapping API Test Case Class"""

    model = Mapping
    brief_fields = [
        "display",
        "id",
        "url",
    ]

    @classmethod
    def setUpTestData(cls) -> None:  # pylint: disable=invalid-name
        """Initial Data setup to perform the 'parent' ViewTestCases"""
        Mapping.objects.create(
            source="https://truc00.com/api", target="http://10.10.10.11:1800/api"
        )
        Mapping.objects.create(
            source="https://truc01.com/api", target="http://10.10.10.12:1800/api"
        )
        Mapping.objects.create(
            source="https://truc02.com/api", target="http://10.10.10.13:1800/api"
        )

        cls.create_data = [
            {
                "source": "https://truc03.com/api",
                "target": "http://10.10.10.14:1800/api",
            },
            {
                "source": "https://truc04.com/api",
                "target": "http://10.10.10.15:1800/api",
            },
            {
                "source": "https://truc05.com/api",
                "target": "http://10.10.10.16:1800/api",
            },
        ]

    def test_that_mapping_is_case_sensitive_unique(self) -> None:
        """Test that mapping is case sensitive unique"""

        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        # pylint: disable=E1101
        obj_perm.users.add(self.user)
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))

        response = self.client.post(
            self._get_list_url(),
            {
                "source": "https://truc10.com/api",
                "target": "http://10.10.10.10:1888/api",
                "authentication": "none",
                "testingpage": None,
            },
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_201_CREATED)

        response = self.client.post(
            self._get_list_url(),
            {
                "source": "https://truc10.com/api",
                "target": "http://10.10.10.10:1888/api",
                "authentication": "none",
                "testingpage": None,
            },
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.content, b'{"source":["mapping with this Source already exists."]}'
        )

        response = self.client.post(
            self._get_list_url(),
            {
                "source": "https://TRUC10.COM/api",
                "target": "http://10.10.10.10:1888/api",
                "authentication": "none",
                "testingpage": None,
            },
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.content, b'{"source":["Mapping with this Source already exists."]}'
        )

        response = self.client.post(
            self._get_list_url(),
            {
                "source": "HTTPS://TRUC10.com/API",
                "target": "HTTP://Toto.10.com:1888/aPi",
                "authentication": "none",
                "testingpage": None,
            },
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_201_CREATED)

        content = json.loads(response.content)

        self.assertEqual(content["source"], "https://truc10.com/API")
        self.assertEqual(content["target"], "http://toto.10.com:1888/aPi")

        response = self.client.get(
            self._get_list_url() + f"{content['id']}/",
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_200_OK)

        content = json.loads(response.content)

        self.assertEqual(content["source"], "https://truc10.com/API")
        self.assertEqual(content["target"], "http://toto.10.com:1888/aPi")

    def test_that_mapping_update_is_case_sensitive_unique(self) -> None:
        """Test that mapping update is case sensitive unique"""

        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        # pylint: disable=E1101
        obj_perm.users.add(self.user)
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))

        response = self.client.post(
            self._get_list_url(),
            {
                "source": "HTTPS://TRUC11.com/API",
                "target": "HTTP://Toto.11.com:1888/aPi",
                "authentication": "none",
                "testingpage": None,
            },
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_201_CREATED)

        content = json.loads(response.content)

        response = self.client.patch(
            self._get_list_url() + f"{content['id']}/",
            {
                "source": "HTTPS://MUCHE11.com/API",
                "target": "HTTP://Titi.11.com:1888/aPi",
            },
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_200_OK)

        content = json.loads(response.content)

        response = self.client.get(
            self._get_list_url() + f"{content['id']}/",
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_200_OK)

        content = json.loads(response.content)

        self.assertEqual(content["source"], "https://muche11.com/API")
        self.assertEqual(content["target"], "http://titi.11.com:1888/aPi")

    def test_samlconfig_defaults_to_null(self) -> None:
        """
        Test that the API returns `null` by default for the `saml_config`
        property of a mapping.
        """

        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        # pylint: disable=E1101
        obj_perm.users.add(self.user)
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))

        mapping = Mapping.objects.create(
            source="https://truc03.com/api",
            target="http://10.10.10.14:1800/api",
        )
        response = self.client.get(
            self._get_list_url() + f"{mapping.id}/",
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_200_OK)

        content = json.loads(response.content)
        self.assertIsNone(content["saml_config"])

    def test_that_mapping_creation_include_proxy_buffer_size_param(self) -> None:
        """Test that mapping creation include proxy buffer size parameter."""

        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        # pylint: disable=E1101
        obj_perm.users.add(self.user)
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))

        response = self.client.post(
            self._get_list_url(),
            {
                "source": "HTTPS://TRUC11.com/API",
                "target": "HTTP://Toto.11.com:1888/aPi",
                "authentication": "none",
                "testingpage": None,
                "proxy_buffer_size": 4,
            },
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_201_CREATED)

        content = json.loads(response.content)

        self.assertIsNotNone(content["proxy_buffer_size"])
        self.assertIsNotNone(content["proxy_buffer"])
        self.assertIsNotNone(content["proxy_busy_buffer"])

    def test_that_mapping_creation_include_proxy_buffer_requests_and_response(
        self,
    ) -> None:
        """Test that mapping creation include proxy buffer size parameter."""

        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        # pylint: disable=E1101
        obj_perm.users.add(self.user)
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))

        response = self.client.post(
            self._get_list_url(),
            {
                "source": "HTTPS://TRUC11.com/API",
                "target": "HTTP://Toto.11.com:1888/aPi",
                "authentication": "none",
                "testingpage": None,
                "proxy_buffer_size": 4,
            },
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_201_CREATED)

        content = json.loads(response.content)

        self.assertIn("proxy_buffer_responses", content)
        self.assertIn("proxy_buffer_requests", content)
