"""Acl Denied Source View Test Case"""

from utilities.testing import ViewTestCases
from netbox_rps_plugin.models import AclDeniedSource, Mapping
from tests.base import BaseModelViewTestCase

class AclDeniedSourceViewTestCase(
    BaseModelViewTestCase,
    ViewTestCases.GetObjectViewTestCase,
    ViewTestCases.GetObjectChangelogViewTestCase,
    # ViewTestCases.CreateObjectViewTestCase,
    # ViewTestCases.EditObjectViewTestCase,
    # ViewTestCases.DeleteObjectViewTestCase,
    # ViewTestCases.ListObjectsViewTestCase,
    # ViewTestCases.BulkImportObjectsViewTestCase,
    # ViewTestCases.BulkDeleteObjectsViewTestCase,
  ):
    """AclDeniedSource API Test Case Class"""

    model = AclDeniedSource

    @classmethod
    def setUpTestData(cls) -> None: # pylint: disable=invalid-name
        """Initial Data setup to perform the 'parent' ViewTestCases"""

        mapping1 = Mapping.objects.create(
            source="https://truc00.com/api", target="http://10.10.10.11:1800/api"
        )
        mapping2 = Mapping.objects.create(
            source="https://truc01.com/api", target="http://10.10.10.12:1800/api"
        )
        mapping3 = Mapping.objects.create(
            source="https://truc02.com/api", target="http://10.10.10.13:1800/api"
        )

        acl_denied_1 = AclDeniedSource.objects.create(
            mapping=mapping1, acl_source="1.2.3.4/32"
        )
        acl_denied_2 = AclDeniedSource.objects.create(
            mapping=mapping2, acl_source="20.21.22.23/24"
        )
        acl_denied_3 = AclDeniedSource.objects.create(
            mapping=mapping3, acl_source="1:2:3:4:5:6:7::/32"
        )

        cls.form_data = {
            "acl_source": "28.29.30.31/32"
        }

        cls.csv_update_data = {
            "id,mapping,acl_source",
            f"{acl_denied_1.pk},https://truc03.com/api,4.3.2.1/32",
            f"{acl_denied_2.pk},https://truc04.com/api,40.30.20.10/32",
            f"{acl_denied_3.pk},https://truc05.com/api,7:6:5:4:3:2:1::/32",
        }

        cls.csv_data = (
            "id,acl_source",
            f"{acl_denied_1.pk},4.3.2.1/32",
            f"{acl_denied_2.pk},40.30.20.10/32",
            f"{acl_denied_3.pk},7:6:5:4:3:2:1::/32",
        )

        pass  # pylint: disable=unnecessary-pass

    def tearDown(self) -> None:# pylint: disable=invalid-name
        """Method called immediately after the test method has been called and the result recorded."""
        Mapping.objects.all().delete()
        AclDeniedSource.objects.all().delete()
        super().tearDown()
