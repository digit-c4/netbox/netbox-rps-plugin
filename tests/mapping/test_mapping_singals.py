"Signals test suit"

from unittest.mock import patch
from django.test import TestCase
from netbox_rps_plugin.models import (
    HttpHeader,
    SamlConfig,
    AclDeniedSource,
    CacheConfig,
    HstsProtocol,
    Mapping,
)


class SignalTestCase(TestCase):
    "Signals test suit"

    @classmethod
    def setUpTestData(cls) -> None:  # pylint: disable=invalid-name
        acs_url_tmp = "https://truc11.com/api"
        logout_url_tmp = "http://10.10.10.21:1800/api"
        test_mapping = Mapping.objects.create(
            source="https://truc00.com/api", target="http://10.10.10.10:1800/api"
        )

        cls.test_data = [
            HttpHeader(name="Test http header name", mapping=test_mapping),
            SamlConfig(
                acs_url=acs_url_tmp,
                logout_url=logout_url_tmp,
                force_nauth=False,
                mapping=test_mapping,
            ),
            AclDeniedSource(
                mapping=test_mapping,
                acl_source="1.2.3.4/12",
            ),
            CacheConfig(
                mapping=test_mapping,
                list_extensions=["JPG"],
            ),
            HstsProtocol(
                max_age=0,
                subdomains=True,
                preload_flag=True,
                mapping=test_mapping,
            ),
        ]

    def test_signal_callback_triggered_on_create(self):
        """
        Test that the signal callback is triggered and instance.mapping.save()
        is calledwhen one of the sub_resources is created
        (and the post_save signal is automatically triggered).
        """
        for instance in self.test_data:
            with patch.object(instance.mapping, "save") as mock_save:
                instance.save()
                mock_save.assert_called_once()

    def test_signal_callback_triggered_on_update(self):
        """
        Test that the signal callback is triggered and instance.mapping.save()
        is called when one of the sub_resources is updated
        (and the post_save signal is automatically triggered).
        """
        for instance in self.test_data:
            if isinstance(instance, HttpHeader):
                instance.name = "updated name"

            elif isinstance(instance, SamlConfig):
                instance.logout_url = "http://10.10.10.21:1800/api/logout"

            elif isinstance(instance, AclDeniedSource):
                instance.acl_source = "1.2.3.4/14"

            elif isinstance(instance, CacheConfig):
                instance.list_extensions = (["PNG"],)

            elif isinstance(instance, HstsProtocol):
                instance.max_age = 10
            else:
                raise ValueError("Unsupported sub_resource type")

            with patch.object(instance.mapping, "save") as mock_save:
                instance.save()
                mock_save.assert_called_once()
