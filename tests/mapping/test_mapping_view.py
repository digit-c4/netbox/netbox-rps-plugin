"""Mapping Views Test Case"""

from utilities.testing import ViewTestCases
from utilities.testing.utils import post_data
from users.models import ObjectPermission
from django.contrib.contenttypes.models import ContentType
from tests.base import BaseModelViewTestCase
from netbox_rps_plugin.models import Mapping


class MappingViewsTestCase(
    BaseModelViewTestCase,
    ViewTestCases.GetObjectViewTestCase,
    ViewTestCases.GetObjectChangelogViewTestCase,
    ViewTestCases.CreateObjectViewTestCase,
    ViewTestCases.EditObjectViewTestCase,
    ViewTestCases.DeleteObjectViewTestCase,
    ViewTestCases.ListObjectsViewTestCase,
    ViewTestCases.BulkImportObjectsViewTestCase,
    ViewTestCases.BulkDeleteObjectsViewTestCase,
):

    """Mapping Views Test Case Class"""

    model = Mapping

    @classmethod
    def setUpTestData(cls):  # pylint: disable=invalid-name
        """Initial Data setup to perform the 'parent' ViewTestCases"""
        mapping1 = Mapping.objects.create(
            source="https://truc00.com/api", target="http://10.10.10.11:1800/api"
        )
        mapping2 = Mapping.objects.create(
            source="https://truc01.com/api", target="http://10.10.10.12:1800/api"
        )
        mapping3 = Mapping.objects.create(
            source="https://truc02.com/api", target="http://10.10.10.13:1800/api"
        )
        cls.mapping4 = Mapping.objects.create(
            source="https://truc10.com/api", target="http://10.10.10.14:1800/api"
        )

        cls.form_data = {
            "source": "https://truc05.com/api",
            "target": "http://10.10.10.15:1800/api",
            "authentication": "ldap",
            "keepalive_requests": 600,
            "keepalive_timeout": 10,
            "proxy_read_timeout": 60,
            "client_max_body_size": 512,
            "sorry_page": "http://10.10.10.15:1800/sorry_page",
            "proxy_buffer_size": 32,
        }

        cls.csv_update_data = (
            "id,source,target,keepalive_requests,keepalive_timeout,"
                "proxy_read_timeout,client_max_body_size,sorry_page",
            f"{mapping1.pk},https://truc07.com/api,http://10.10.10.11:1800/new_api,\
                600,10,60,512,https://10.10.10.15:1800/sorry_page",
            f"{mapping2.pk},https://truc08.com/api,http://10.10.10.12:1800/new_api,\
                600,10,60,1024,https://10.10.10.15:1800/sorry_page",
            f"{mapping3.pk},https://truc09.com/api,http://10.10.10.13:1800/new_api,\
                600,10,60,2048,https://10.10.10.15:1800/sorry_page",
        )

        cls.csv_data = (
            "source,target",
            "https://truc04.com/api,https://truc039.com/api",
            "https://truc05.com/api,https://truc069.com/api",
            "https://truc06.com/api,https://truc049.com/api",
        )

    def test_that_data_with_three_optional_proxies_is_ok(self):
        """ Test that a request with the three proxy buffer is functionning as expected. """

        self.form_data = {
            "source": "https://truc05.com/api",
            "target": "http://10.10.10.15:1800/api",
            "authentication": "ldap",
            "keepalive_requests": 600,
            "keepalive_timeout": 10,
            "proxy_read_timeout": 60,
            "client_max_body_size": 512,
            "sorry_page": "http://10.10.10.15:1800/sorry_page",
            "proxy_buffer_size": 32,
            "proxy_buffer": 128,
            "proxy_busy_buffer": 64,
        }

        # Info : Assign model-level permission
        obj_perm = ObjectPermission(
            name='Test permission',
            actions=['change']
        )
        obj_perm.save()
        obj_perm.users.add(self.user)  # pylint: disable=no-member
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))  # pylint: disable=no-member

        # Try GET with model-level permission
        self.assertHttpStatus(self.client.get(self._get_url('edit', self.mapping4)), 200)

        # Try POST with model-level permission
        request = {
            'path': self._get_url('edit', self.mapping4),
            'data': post_data(self.form_data),
        }
        self.assertHttpStatus(self.client.post(**request), 302)
        instance = self._get_queryset().get(pk=self.mapping4.pk)
        self.assertInstanceEqual(instance, self.form_data, exclude=self.validation_excluded_fields)

        # # Verify ObjectChange creation
        # if issubclass(instance.__class__, ChangeLoggingMixin):
        #     objectchanges = ObjectChange.objects.filter(
        #         changed_object_type=ContentType.objects.get_for_model(instance),
        #         changed_object_id=instance.pk
        #     )
        #     self.assertEqual(len(objectchanges), 1)
        #     self.assertEqual(objectchanges[0].action, ObjectChangeActionChoices.ACTION_UPDATE)

    # Section : Teardown
    def tearDown(self) -> None:# pylint: disable=invalid-name
        """Method called immediately after the test method has been called and the result recorded."""
        Mapping.objects.all().delete()
        super().tearDown()
