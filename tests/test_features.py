"""Plugin Features Test Case"""

from utilities.testing.api import APITestCase  # pylint: disable=import-error

from netbox_rps_plugin.models import Mapping, MaxBodyChoices

class FeaturesTestCase(APITestCase):
    """Plugin Features Test Case"""

    def test_that_client_max_body_size_have_specific_choices_only(self):
        """Test that client max body size have only specific value
        to be selected"""
        # Section : Test MaxBodyChoices Class new specifications.
        test_maxbodychoices = MaxBodyChoices()
        self.assertEqual(len(test_maxbodychoices), 5)
        self.assertEqual(len(test_maxbodychoices[0]), 2)

        # Section : Test Mapping Class Update specifications.
        # Test also backward compatibility.
        # Info : May need some adaptation.
        test_mapping = Mapping()
        test_mapping.client_max_body_size = 42
        self.assertIsNotNone(test_mapping.client_max_body_size)
        self.assertEqual(type(test_mapping.client_max_body_size), int)
