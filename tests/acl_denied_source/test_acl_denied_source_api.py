"""Host API Test Case"""
import json
from rest_framework import status
from users.models import ObjectPermission # pylint: disable=import-error
from django.contrib.contenttypes.models import ContentType

from netbox_rps_plugin.models import AclDeniedSource, Mapping
from tests.base import BaseAPITestCase

class AclDeniedSourceApiTestCase(
    BaseAPITestCase,
  ):
    """AclDeniedSource API Test Case Class"""

    model = AclDeniedSource

    @classmethod
    def setUpTestData(cls) -> None: # pylint: disable=invalid-name
        """Initial Data setup to perform the 'parent' ViewTestCases"""
        pass  # pylint: disable=unnecessary-pass

    def test_add_acl_denied_source(self):
        """Test adding ACL denied source"""

        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        # pylint: disable=E1101
        obj_perm.users.add(self.user)
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))
        mapping = Mapping.objects.create(
            source="https://trucmuch10.com/api", target="http://10.10.10.42:1888/api"
        )

        data = {
            "mapping" : mapping.pk,
            "acl_source" : "1.2.3.4/12",
            "last_updated" : "2024-03-15 10:15:43.111903+00",
            "created" : "2024-03-15 10:15:43.111903+00",
        }
        response = self.client.post(self._get_list_url(), data, format="json", **self.header)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue("id" in response.data)
        self.assertEqual(response.data["acl_source"], "1.2.3.4/12")

    def tearDown(self) -> None:# pylint: disable=invalid-name
        """Method called immediately after the test method has been called and the result recorded."""
        Mapping.objects.all().delete()
        AclDeniedSource.objects.all().delete()
        super().tearDown()

    def test_edit_acl_denied_source(self):
        """Test editing ACL denied source"""

        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        # pylint: disable=E1101
        obj_perm.users.add(self.user)
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))
        mapping = Mapping.objects.create(
            source="https://truc10.com/api", target="http://10.10.10.10:1888/api"
        )

        data = {
            "mapping" : mapping.pk,
            "acl_source" : "1.2.3.4/12",
            "last_updated" : "2024-03-15 10:15:43.111903+00",
            "created" : "2024-03-15 10:15:43.111903+00",
        }
        response = self.client.post(self._get_list_url(), data, format="json", **self.header)

        acl = json.loads(response.content)

        data = {
            "mapping" : mapping.pk,
            "acl_source" : "2.2.3.4/12",
        }
        response = self.client.patch(self._get_list_url() + f"{acl['id']}/", data,
            format="json", **self.header)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue("id" in response.data)
        self.assertEqual(response.data["acl_source"], "2.2.3.4/12")

    def test_delete_acl_denied_source(self):
        """Test deleting ACL denied source"""

        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view", "delete"],
        )
        obj_perm.save()
        # pylint: disable=E1101
        obj_perm.users.add(self.user)
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))
        mapping = Mapping.objects.create(
            source="https://truc10.com/api", target="http://10.10.10.10:1888/api"
        )

        data = {
            "mapping" : mapping.pk,
            "acl_source" : "1.2.3.4/12",
            "last_updated" : "2024-03-15 10:15:43.111903+00",
            "created" : "2024-03-15 10:15:43.111903+00",
        }
        response = self.client.post(self._get_list_url(), data, format="json", **self.header)

        acl = json.loads(response.content)

        response = self.client.delete(self._get_list_url() + f"{acl['id']}/", **self.header)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
