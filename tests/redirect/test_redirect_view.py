"""Redirect VIEW Test Case"""
from datetime import datetime, timedelta
import pytz
from django.contrib.contenttypes.models import ContentType  # pylint: disable=import-error
from django.test import override_settings # pylint: disable=import-error
from users.models import ObjectPermission  # pylint: disable=import-error
from utilities.testing import ViewTestCases # pylint: disable=import-error
from utilities.testing.utils import post_data # pylint: disable=import-error
from netbox_rps_plugin.models import Mapping, Redirect # pylint: disable=import-error
from tests.base import BaseModelViewTestCase

class RedirectViewTestCase(
    BaseModelViewTestCase,
    ViewTestCases.GetObjectViewTestCase,
    ViewTestCases.ListObjectsViewTestCase,
    ViewTestCases.CreateObjectViewTestCase,
    ViewTestCases.DeleteObjectViewTestCase,
):
    """Redirect VIEW Test Case Class"""

    model = Redirect

    @classmethod
    def setUpTestData(cls) -> None:
        """Initial Data setup to perform the 'parent' ViewTestCases"""
        cls.mapp00 = Mapping.objects.create(
            source="https://source.testmapping000.url", target="https://target.testmapping000.url"
        )
        cls.mapp01 = Mapping.objects.create(
            source="https://source.testmapping001.url", target="https://target.testmapping001.url"
        )
        cls.mapp02 = Mapping.objects.create(
            source="https://source.testmapping002.url", target="https://target.testmapping002.url"
        )
        cls.mapp03 = Mapping.objects.create(
            source="https://source.testmapping003.url", target="https://target.testmapping003.url"
        )

        cls.redir00 = Redirect.objects.create(
            source="https://source.testmapping000.url/redir00",
            target="http://target.testredirect000.url",
            mapping=cls.mapp00,
        )
        cls.redir01 = Redirect.objects.create(
            source="https://source.testmapping001.url/redir01",
            target="http://target.testredirect001.url",
            mapping=cls.mapp01,
        )
        cls.redir02 = Redirect.objects.create(
            source="https://source.testmapping002.url/redir02",
            target="http://target.testredirect002.url",
            mapping=cls.mapp02,
        )

        cls.form_data = {
            "mapping": cls.mapp03.pk,
            "source": "https://source.testmapping003.url/redir06",
            "target": "http://target.testredirect006.url",
            "activation_date": datetime.now(pytz.timezone('Europe/Luxembourg')),
            "redirect_type": "1-1",
        }
        cls.csv_data = (
            "source,target,mapping,redirect_type",
            f"https://source.testmapping000.url/redir04,http://target.testredirect004.url,{cls.mapp00.pk},N-N",
            f"https://source.testmapping000.url/redir05,http://target.testredirect005.url,{cls.mapp00.pk},N-N",
            f"https://source.testmapping000.url/redir06,http://target.testredirect006.url,{cls.mapp00.pk},N-N",
        )
        cls.csv_update_data = (
            "id,mapping,redirect_type",
            f"{cls.redir00.pk},{cls.mapp00.pk},1-1",
            f"{cls.redir01.pk},{cls.mapp01.pk},1-1",
            f"{cls.redir02.pk},{cls.mapp02.pk},1-1",
        )

        cls.bulk_edit_data = {"redirect_type": "N-N", "mapping": cls.mapp01.pk}


    @override_settings(EXEMPT_VIEW_PERMISSIONS=['*'])
    def test_activation_deactivation_date_constraint(self):
        """Test that deactivation_date is after activation_date"""

        # Assign unconstrained permission
        obj_perm = ObjectPermission(
            name='Test permission',
            actions=['add']
        )
        obj_perm.save()
        obj_perm.users.add(self.user) # pylint: disable=E1101
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model)) # pylint: disable=E1101

        # Try GET with model-level permission
        self.assertHttpStatus(self.client.get(self._get_url('add')), 200)

        # Try POST with model-level permission
        initial_count = self._get_queryset().count()
        request = {
            'path': self._get_url('add'),
            'data': post_data({
                "mapping": self.mapp03.pk,
                "source": "https://source.testmapping003.url/redir06",
                "target": "http://target.testredirect066.url",
                "activation_date": datetime.now(pytz.timezone('Europe/Luxembourg')),
                "deactivation_date": datetime.now(pytz.timezone('Europe/Luxembourg')) - timedelta(days=7),
                "redirect_type": "1-1",
            }),
        }
        self.assertHttpStatus(self.client.post(**request), 200)
        self.assertEqual(initial_count, self._get_queryset().count())

    @override_settings(EXEMPT_VIEW_PERMISSIONS=['*'])
    def test_overlapping_redirects(self):
        """Checking timespan constraints between Redirects ViewTestcase"""

        # Assign unconstrained permission
        obj_perm = ObjectPermission(
            name='Test permission',
            actions=['add']
        )
        obj_perm.save()
        obj_perm.users.add(self.user) # pylint: disable=E1101
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model)) # pylint: disable=E1101

        # Try GET with model-level permission
        self.assertHttpStatus(self.client.get(self._get_url('add')), 200)

        # Test creating a VALID Redirect instance
        request = {
            'path': self._get_url('add'),
            'data': post_data({
                "mapping": self.mapp03.pk,
                "source": "https://source.testmapping003.url/redir66",
                "target": "http://target.testredirect066.url",
                "activation_date": datetime.now(pytz.timezone('Europe/Luxembourg')),
                "deactivation_date": datetime.now(pytz.timezone('Europe/Luxembourg')) + timedelta(days=7),
                "redirect_type": "1-1",
            }),
        }
        self.assertHttpStatus(self.client.post(**request), 302)

        # Test creating a INVALID Redirect instance
        # Same source, same timespan
        request = {
            'path': self._get_url('add'),
            'data': post_data({
                "mapping": self.mapp03.pk,
                "source": "https://source.testmapping003.url/redir66",
                "target": "http://target.testredirect076.url",
                "activation_date": datetime.now(pytz.timezone('Europe/Luxembourg')) - timedelta(days=7),
                "deactivation_date": datetime.now(pytz.timezone('Europe/Luxembourg')) + timedelta(days=14),
                "redirect_type": "1-1",
            }),
        }
        self.assertHttpStatus(self.client.post(**request), 200)
