"""Host API Test Case"""

import json
from datetime import datetime, timedelta
import pytz
from django.contrib.contenttypes.models import ContentType  # pylint: disable=import-error
from users.models import ObjectPermission  # pylint: disable=import-error
from rest_framework import status  # pylint: disable=import-error
from utilities.testing import APIViewTestCases # pylint: disable=import-error
from netbox_rps_plugin.models import Mapping, Redirect, RedirectTypeChoices # pylint: disable=import-error
from tests.base import BaseAPITestCase


class RedirectApiTestCase(
    BaseAPITestCase,
    APIViewTestCases.GetObjectViewTestCase,
    APIViewTestCases.ListObjectsViewTestCase,
    APIViewTestCases.CreateObjectViewTestCase,
    APIViewTestCases.UpdateObjectViewTestCase,
    APIViewTestCases.DeleteObjectViewTestCase,
):
    """Redirect API Test Case Class"""

    model = Redirect
    brief_fields = [
        "activation_date",
        "deactivation_date",
        "id",
        "mapping",
        "redirect_type",
        "source",
        "target",
        "url"
    ]

    @classmethod
    def setUpTestData(cls) -> None: # pylint: disable=invalid-name
        """Initial Data setup to perform the 'parent' ApiTestCases"""
        mapping_001 = Mapping.objects.create(
            source = "https://source.testmapping001.url",
            target = "https://target.testmapping001.url"
        )

        Redirect.objects.create(
            mapping = mapping_001,
            source = "https://source.testmapping001.url/redir01",
            target = "http://target.testredirect001.url",
            redirect_type = "1-1"
        )

        Redirect.objects.create(
            mapping = mapping_001,
            source = "https://source.testmapping001.url/redir02",
            target = "http://target.testredirect002.url",
            redirect_type = "1-1"
        )

        Redirect.objects.create(
            mapping = mapping_001,
            source = "https://source.testmapping001.url/redir03",
            target = "http://target.testredirect001.url",
            redirect_type = "1-1"
        )

        cls.create_data = [
            {
                "mapping": mapping_001.pk,
                "source": "https://source.testmapping001.url/redir04",
                "target": "http://target.testredirect004.url",
                "redirect_type": "N-N"
            },
            {
                "mapping": mapping_001.pk,
                "source": "https://source.testmapping001.url/redir05",
                "target": "http://target.testredirect005.url",
                "redirect_type": "N-N"
            },
            {
                "mapping": mapping_001.pk,
                "source": "https://source.testmapping001.url/redir06",
                "target": "http://target.testredirect006.url",
                "redirect_type": "N-N"
            }
        ]

    def test_deactivation_date_gt_activation_date(self) -> None:
        """Checking constraints between activation date & deactivation date APITestcase"""

        mapping_003 = Mapping.objects.create(
            source = "https://source.testmapping003.url",
            target = "https://target.testmapping003.url"
        )

        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        # pylint: disable=E1101
        obj_perm.users.add(self.user)
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))

        # Test creating a INVALID Redirect instance
        # Deactivation date BEFORE activation date
        response = self.client.post(
            self._get_list_url(),
            {
                "mapping": mapping_003.id,
                "source": "https://source.testmapping003.url",
                "target": "https://target.testmapping003.url",
                "redirect_type": RedirectTypeChoices.DEFAULT_VALUE,
                "activation_date": datetime.now(pytz.timezone('Europe/Luxembourg')),
                "deactivation_date": datetime.now(pytz.timezone('Europe/Luxembourg')) - timedelta(days=7)
            },
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_400_BAD_REQUEST)

        # Creating a valid Redirect instance
        response = self.client.post(
            self._get_list_url(),
            {
                "mapping": mapping_003.id,
                "source": "https://source.testmapping003.url",
                "target": "https://target.testmapping003.url",
                "redirect_type": RedirectTypeChoices.DEFAULT_VALUE,
                "activation_date": datetime.now(pytz.timezone('Europe/Luxembourg')),
                "deactivation_date": datetime.now(pytz.timezone('Europe/Luxembourg')) + timedelta(days=7)
            },
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_201_CREATED)
        content = json.loads(response.content)

        # Test updating a Redirect instance with INVALID data
        # Deactivation date BEFORE activation date
        response = self.client.patch(
            self._get_list_url() + f"{content['id']}/",
            {
                "mapping": mapping_003.id,
                "source": "https://updatedsource.testmapping001.url",
                "target": "https://updatedtarget.testmapping001.url",
                "redirect_type": RedirectTypeChoices.DEFAULT_VALUE,
                "activation_date": datetime.now(pytz.timezone('Europe/Luxembourg')),
                "deactivation_date": datetime.now(pytz.timezone('Europe/Luxembourg')) + timedelta(days=-7)
            },
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_400_BAD_REQUEST)

    def test_regex_characters_allowed(self) -> None:
        """REGEX characters allowed APITestcase"""

        mapping_004 = Mapping.objects.create(
            source = "https://source.testmapping004.url",
            target = "https://target.testmapping004.url"
        )

        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        # pylint: disable=E1101
        obj_perm.users.add(self.user)
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))

        # Test creating a VALID Redirect instance
        # Source with characters commonly used in REGEXES
        response = self.client.post(
            self._get_list_url(),
            {
                "mapping": mapping_004.id,
                "source": "https://source.testmapping004.url/^$*+?[]|(){}\\d\\w\\s",
                "target": "https://target.testmapping004.url/^$*+?[]|(){}\\d\\w\\s",
                "redirect_type": RedirectTypeChoices.DEFAULT_VALUE,
                "activation_date": datetime.now(pytz.timezone('Europe/Luxembourg')),
            },
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_201_CREATED)
        content = json.loads(response.content)

        # Test updating a VALID Redirect instance
        # Source with characters commonly used in REGEXES
        response = self.client.patch(
            self._get_list_url() + f"{content['id']}/",
            {
                "mapping": mapping_004.id,
                "source": "https://source.testmapping004.url/^$*+?[]|(){}\\d\\w\\s",
                "target": "https://updated.target.testmapping004.url/^$*+?[]|(){}\\d\\w\\s",
                "redirect_type": RedirectTypeChoices.DEFAULT_VALUE,
                "activation_date": datetime.now(pytz.timezone('Europe/Luxembourg')),
            },
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_200_OK)

    def test_overlapping_redirects(self) -> None:
        """Checking timespan constraints between Redirects APITestcase"""

        mapping_005 = Mapping.objects.create(
            source = "https://source.testmapping005.url",
            target = "https://target.testmapping005.url"
        )

        # Setting a base redirect, which will or will not generate conflict
        Redirect.objects.create(
            mapping = mapping_005,
            source = "https://source.testmapping005.url/redir010",
            target = "http://target.testredirect010.url",
            redirect_type = "1-1",
            activation_date = datetime.now(pytz.timezone('Europe/Luxembourg')),
            deactivation_date = datetime.now(pytz.timezone('Europe/Luxembourg')) + timedelta(days=7)
        )

        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        # pylint: disable=E1101
        obj_perm.users.add(self.user)
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))

        # Test creating a INVALID Redirect instance
        # Same source, same timespan
        response = self.client.post(
            self._get_list_url(),
            {
                "mapping": mapping_005.id,
                "source": "https://source.testmapping005.url/redir010",
                "target": "https://target.testmapping010.url",
                "redirect_type": RedirectTypeChoices.DEFAULT_VALUE,
                "activation_date": datetime.now(pytz.timezone('Europe/Luxembourg')),
                "deactivation_date": datetime.now(pytz.timezone('Europe/Luxembourg')) + timedelta(days=7)
            },
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_400_BAD_REQUEST)

        # Test creating a INVALID Redirect instance
        # Same source, starting before, no deactivation date
        response = self.client.post(
            self._get_list_url(),
            {
                "mapping": mapping_005.id,
                "source": "https://source.testmapping005.url/redir010",
                "target": "https://target.testmapping010.url",
                "redirect_type": RedirectTypeChoices.DEFAULT_VALUE,
                "activation_date": datetime.now(pytz.timezone('Europe/Luxembourg')) - timedelta(days=7)
            },
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_400_BAD_REQUEST)

        # Test creating a VALID Redirect instance
        # Same source, starting before, ending before
        response = self.client.post(
            self._get_list_url(),
            {
                "mapping": mapping_005.id,
                "source": "https://source.testmapping005.url/redir010",
                "target": "https://target.testmapping010.url/redir010",
                "redirect_type": RedirectTypeChoices.DEFAULT_VALUE,
                "activation_date": datetime.now(pytz.timezone('Europe/Luxembourg')) - timedelta(days=7),
                "deactivation_date": datetime.now(pytz.timezone('Europe/Luxembourg')) - timedelta(days=1)
            },
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_201_CREATED)
        content = json.loads(response.content)

        # Test updating a Redirect instance with INVALID data
        # Same source, overlapping timespan
        response = self.client.patch(
            self._get_list_url() + f"{content['id']}/",
            {
                "mapping": mapping_005.id,
                "source": "https://source.testmapping005.url/redir010",
                "target": "https://target.testmapping010.url/redir010",
                "redirect_type": RedirectTypeChoices.DEFAULT_VALUE,
                "activation_date": datetime.now(pytz.timezone('Europe/Luxembourg')),
                "deactivation_date": datetime.now(pytz.timezone('Europe/Luxembourg')) + timedelta(days=14)
            },
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_400_BAD_REQUEST)
